/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : cakephp

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-11-18 08:03:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `authors`
-- ----------------------------
DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of authors
-- ----------------------------
INSERT INTO `authors` VALUES ('1', 'Thai Lai', 'thailvn@gmail.com', '1416208918', '1416208918');

-- ----------------------------
-- Table structure for `books`
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(13) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `total_page` int(10) DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `author_id` int(10) DEFAULT '0',
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `isbn_unique` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES ('2', 'c,c++', 'Lập trình C', 'Lap trinh C can ban', '', '0', '2014/11/16/7c9720d7640c0f6b4bcd0aa426af6c17%s.jpg', '1', null, '1');
INSERT INTO `books` VALUES ('3', 'C++', 'Cấu trúc dữ liệu và giả thuật', 'Cau truc du lieu va giai thuat', '', '0', '2014/11/16/b7a659c7be1eceff15cd13524291aeba%s.jpg', '1', null, '1');
INSERT INTO `books` VALUES ('4', 'php', 'PHP căn bản', 'php, lap trinh php', 'http://book.cakephp.org/', '0', '2014/11/16/e968e17c6180ed360be35e4cde26c880%s.jpg', '0', null, '0');
INSERT INTO `books` VALUES ('5', 'php nc', 'PHP Nâng cao　２', 'php,php nang cao', 'http://book.cakephp.org/', '0', '2014/11/16/5ba3c88bfd883ca89f5ac210f52c4da6%s.png', '1', null, '0');
INSERT INTO `books` VALUES ('6', 'php fw', 'PHP Framework', 'php, php framework', '', '0', '2014/11/16/9683b193e04b3872258c7b061f0f1d48%s.jpg', '1', null, '0');
INSERT INTO `books` VALUES ('24', 'abc 2', 'Lập trình ứng dụng web tập II', 'abc', '', '0', '2014/11/16/6d67bea5fa9568a9dcd77e2fd3a9ff6a%s.jpg', '1', null, '0');
INSERT INTO `books` VALUES ('26', 'abc 3', 'Lập trình web với CakePHP', 'abc', '', '0', '2014/11/16/f56912094466dd87a7635bebb584af72%s.png', '1', null, '0');
INSERT INTO `books` VALUES ('38', 'ddd', 'dddd', 'ddd', '', '0', '2014/11/16/dba6634ffb307c03f065476938d6ff94%s.jpg', '1', '2014-11-14 03:43:14', '0');
INSERT INTO `books` VALUES ('43', 'ssss', 'sss', 'ss', '', '0', '2014/11/16/2173e96d8bf5e41a395feaf162d25473%s.jpg', '1', '2014-11-16 04:33:27', '0');
INSERT INTO `books` VALUES ('44', 'dddddd', 'dddd', 'ddd', '', '0', '2014/11/16/a168a9dd969a3eb65b18cad54187bd73%s.jpg', '1', '2014-11-16 04:33:40', '0');
INSERT INTO `books` VALUES ('45', 'sass', 'ddsds', 'dds', '', '0', null, '1', '2014-11-16 04:33:47', '0');
INSERT INTO `books` VALUES ('46', 'dssfdff', 'ff', 'fdfdfd', '', '0', null, '1', '2014-11-16 04:33:53', '0');
INSERT INTO `books` VALUES ('47', '7777', '777', '7777', '', '0', null, '1', '2014-11-16 05:59:13', '0');
INSERT INTO `books` VALUES ('49', 'ddddeee', 'ddff', 'ffc', '', '0', '2014/11/16/bbab73f56fc6b5ee79da06cb886e69eb%s.jpg', '1', '2014-11-16 06:00:16', '0');
INSERT INTO `books` VALUES ('50', 'xxx', 'xxx', 'xxx', '', '0', '2014/11/16/0a1ff3c1f7c6084384242bf8032069f0%s.jpg', '1', '2014-11-16 06:01:47', '0');
INSERT INTO `books` VALUES ('51', 'ddddddd', 'ddd', 'dddd', '', '0', null, '1', '2014-11-17 02:49:16', '0');

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `type` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `migration` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('app', 'default', '001_create_authors');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(13) NOT NULL,
  `email` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'abcxyz', 'Kĩ thuật lập trình');
INSERT INTO `users` VALUES ('2', 'c,c++', 'Lập trình C');
INSERT INTO `users` VALUES ('3', 'C++', 'Cấu trúc dữ liệu và giả thuật');
INSERT INTO `users` VALUES ('4', 'php', 'PHP căn bản');
INSERT INTO `users` VALUES ('5', 'thailvn', 'thailvn@gmail.com');
INSERT INTO `users` VALUES ('6', 'php fw', 'PHP Framework');
INSERT INTO `users` VALUES ('22', 'abc 1', 'Lập trình ứng dụng web tập I');
INSERT INTO `users` VALUES ('24', 'abc 2', 'Lập trình ứng dụng web tập II');
INSERT INTO `users` VALUES ('26', 'abc 3', 'Lập trình web với CakePHP');
INSERT INTO `users` VALUES ('27', 'acb 4', 'Lập trình web với CodeIgniter');
INSERT INTO `users` VALUES ('28', 'abc 5', 'Tutorial covered by Thái Thanh Phong ^0^');
INSERT INTO `users` VALUES ('29', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('30', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('31', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('32', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('33', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('34', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('35', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('36', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('37', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('38', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('39', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('40', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('41', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('42', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('43', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('44', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('45', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('46', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('47', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('48', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('49', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('50', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('51', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('52', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('53', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('54', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('55', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('56', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('57', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('58', 'Atem', 'atem.vn@gmail.com');
INSERT INTO `users` VALUES ('59', 'BaoLai', 'lainguyenbao@gmail.com');
INSERT INTO `users` VALUES ('60', 'Atem', 'atem.vn@gmail.com');
