/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : cakephp

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-11-21 14:06:02
*/

SET FOREIGN_KEY_CHECKS=0; 

-- ----------------------------
-- Table structure for `books`
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(13) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `url` varchar(100) DEFAULT NULL,
  `total_page` int(10) DEFAULT '0',
  `image` varchar(100) DEFAULT NULL,
  `disable` tinyint(1) DEFAULT '1',
  `author_id` int(10) DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  `test_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `isbn_unique` (`isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES ('2', 'dddd６６６６６６６６６', '2222４５５５', '', '', '0', '2014/11/21/64b140794f53d8be960e582f6b0322f7%s.jpg', '1', '1', null, '1416545156', null);
INSERT INTO `books` VALUES ('3', 'C++', 'Cấu trúc dữ liệu và giả thuật', 'Cau truc du lieu va giai thuat', '', '0', '2014/11/16/b7a659c7be1eceff15cd13524291aeba%s.jpg', '1', '1', null, '1416553170', 'あｓｓｄｄｄ');
INSERT INTO `books` VALUES ('4', 'php', 'PHP căn bản', 'php, lap trinh php', 'http://book.cakephp.org/', '0', '2014/11/16/e968e17c6180ed360be35e4cde26c880%s.jpg', '0', '0', null, '1416474897', null);
INSERT INTO `books` VALUES ('5', 'php nc１', 'お先に失礼します', 'おはようございます', 'http://book.cakephp.org/', '0', '2014/11/21/e95f652f7c4fbb4fd0fbc1ebdfd1b877%s.jpg', '0', '0', null, '1416545166', null);
INSERT INTO `books` VALUES ('6', 'php fw', 'PHP Framework', 'php, php framework', '', '0', '2014/11/16/9683b193e04b3872258c7b061f0f1d48%s.jpg', '1', '0', null, '1416549977', null);
INSERT INTO `books` VALUES ('24', 'しつれします', 'ごめんください', 'いがつさいませ\r\nいただきます\r\nコーヒーはいからですか？', 'https://www.goog', '0', '2014/11/21/b06b55f11b3699cb17ca1d5ef4dcabf1%s.jpg', '0', '0', null, '1416540540', null);
INSERT INTO `books` VALUES ('26', 'abc 3', 'Lập trình web với CakePHP', 'abc', '', '0', '2014/11/16/f56912094466dd87a7635bebb584af72%s.png', '1', '0', null, '1416541984', null);
INSERT INTO `books` VALUES ('38', 'ddd', 'dddd', 'ddd', '', '0', '2014/11/16/dba6634ffb307c03f065476938d6ff94%s.jpg', '1', '0', null, '1416541984', null);
INSERT INTO `books` VALUES ('43', 'ssss', 'sss', 'ss', '', '0', '2014/11/16/2173e96d8bf5e41a395feaf162d25473%s.jpg', '0', '0', null, '1416542061', null);
INSERT INTO `books` VALUES ('44', 'ddddddxx', 'dddd', 'ddd', '', '0', '2014/11/21/37581714c6f876b2dba6e2d3d655f64d%s.jpg', '0', '0', null, '1416550068', null);
INSERT INTO `books` VALUES ('45', 'sass', 'ddsds', 'dds', '', '0', null, '1', '0', null, '1416388207', null);
INSERT INTO `books` VALUES ('46', 'dssfdff', 'ff', 'fdfdfd', '', '0', null, '1', '0', null, null, null);
INSERT INTO `books` VALUES ('47', '7777', '777', '7777', '', '0', null, '1', '0', null, null, null);
INSERT INTO `books` VALUES ('49', 'ddddeee', 'ddff', 'ffc', '', '0', '2014/11/16/bbab73f56fc6b5ee79da06cb886e69eb%s.jpg', '1', '0', null, null, null);
INSERT INTO `books` VALUES ('50', 'xxx', 'xxx', 'xxx', '', '0', '2014/11/16/0a1ff3c1f7c6084384242bf8032069f0%s.jpg', '1', '0', null, null, null);
INSERT INTO `books` VALUES ('51', 'ddddddd', 'ddd', 'dddd', '', '0', null, '1', '0', null, null, null);
INSERT INTO `books` VALUES ('52', 'sddddd', 'dddd', '', null, '0', null, '1', '0', null, null, null);
INSERT INTO `books` VALUES ('53', 'hyuyuuu', 'uuuuu', '', '', '0', null, '1', '0', null, '1416391938', null);
INSERT INTO `books` VALUES ('56', 'p0d0d0', '00000f0', '000', '', '0', '2014/11/19/ac33b45a482b9d5ffcdcc01a2670892e%s.jpg', '1', '0', null, '1416381882', null);
INSERT INTO `books` VALUES ('57', 'Ok', 'kdkdkd', 'lflflfl', '', '0', null, '1', '0', '1416381224', '1416381224', null);
INSERT INTO `books` VALUES ('58', 'ABC', '12233', '', '', null, '', null, null, '1416453621', '1416453621', null);
INSERT INTO `books` VALUES ('60', 'ABCD', '111', '111', '', null, '', null, null, '1416453652', '1416453652', null);
INSERT INTO `books` VALUES ('62', 'ABC1', '12233', '', '', null, '', null, null, '1416454033', '1416454033', null);
INSERT INTO `books` VALUES ('65', 'ABC1111', '12233', '', '', null, '', null, null, '1416454065', '1416454065', null);
INSERT INTO `books` VALUES ('67', 'ABC11111', '12233', '', '', null, '', null, null, '1416454104', '1416454104', null);
INSERT INTO `books` VALUES ('68', 'ABC111111', '12233', '', '', null, '', null, null, '1416454108', '1416454108', null);
INSERT INTO `books` VALUES ('70', 'ABC1111111', '12233', '', '', null, '2014/11/21/eeafc899264f83979c6672177da91de6%s.jpg', null, null, '1416454179', '1416543478', null);
INSERT INTO `books` VALUES ('72', 'ABC11111111', '12233', '', '', null, '', null, null, '1416454268', '1416454268', null);
INSERT INTO `books` VALUES ('73', 'ABC21111111', '12233', '', '', null, '', null, null, '1416454374', '1416454374', null);
INSERT INTO `books` VALUES ('75', 'AB1C21111111', '12233', '', '', null, '', null, null, '1416454573', '1416454573', null);
INSERT INTO `books` VALUES ('76', 'AB12111111', '12233', '', '', null, '2014/11/21/c930ec74997c133655d302c4aa87eef5%s.jpg', null, null, '1416454578', '1416543242', null);
INSERT INTO `books` VALUES ('78', 'AB12111', '12233', '', '', null, '', null, null, '1416454614', '1416454614', null);
INSERT INTO `books` VALUES ('80', 'AB3', '12233', '', '', null, '', null, null, '1416454661', '1416454661', null);
INSERT INTO `books` VALUES ('82', 'XX1', '12233', '', '', null, '', null, null, '1416454699', '1416454699', null);
INSERT INTO `books` VALUES ('83', 'XX12', '12233', '', '', null, '', null, null, '1416454766', '1416454766', null);
INSERT INTO `books` VALUES ('84', 'Ok1', 'Ok 1', 'vnmm', '', null, '', null, null, '1416454814', '1416454814', null);
INSERT INTO `books` VALUES ('85', '11122', '233', '33', '', null, '', null, null, '1416454847', '1416454847', null);
INSERT INTO `books` VALUES ('86', '555', '6', '666', '', null, '', null, null, '1416454873', '1416454873', null);
INSERT INTO `books` VALUES ('87', '5554', '6', '666', '', null, '', null, null, '1416454899', '1416454899', null);
INSERT INTO `books` VALUES ('88', 'dddddxxx', 'xxx', 'xx', '', null, '', '0', null, '1416455204', '1416473244', null);
INSERT INTO `books` VALUES ('89', '１１１１', '１１１１', '', '', null, '2014/11/21/4c66b1cde81296a3abbee9e605a923bc%s.jpg', '1', null, '1416465550', '1416545116', null);
INSERT INTO `books` VALUES ('92', 'dddｄｘ', '2222４５５５', '', '', null, '', null, null, '1416477660', '1416477660', null);
INSERT INTO `books` VALUES ('95', 'dddzzz', '2222４５５５', '', '', null, '2014/11/20/25ec372bab6329d94f3029858be8b754%s.png', '0', null, '1416477688', '1416478063', null);
INSERT INTO `books` VALUES ('96', 'Test jdsjd', 'ｄｄｄ', 'ｄｄ', '', null, '2014/11/21/94b4556e245a08431b01d62feead6d6f%s.jpg', '0', null, '1416542227', '1416545058', null);
INSERT INTO `books` VALUES ('97', 'ｄｄｄ', 'ｄｄ', 'ｄｄｄｄ', '', null, null, null, null, '1416551977', '1416551977', null);
DROP TRIGGER IF EXISTS `before_insert_books`;
DELIMITER ;;
CREATE TRIGGER `before_insert_books` BEFORE INSERT ON `books` FOR EACH ROW SET 
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `before_update_books`;
DELIMITER ;;
CREATE TRIGGER `before_update_books` BEFORE UPDATE ON `books` FOR EACH ROW SET new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
