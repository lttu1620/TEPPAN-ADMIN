/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : cakephp

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-11-27 09:35:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `user_facebook_informations`
-- ----------------------------
DROP TABLE IF EXISTS `user_facebook_informations`;
CREATE TABLE `user_facebook_informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT 'ユーザID',
  `facebook_id` varchar(255) NOT NULL,
  `facebook_username` varchar(100) DEFAULT NULL,
  `facebook_email` varchar(100) DEFAULT NULL,
  `facebook_first_name` varchar(100) DEFAULT NULL,
  `facebook_last_name` varchar(100) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `facebook_image` varchar(255) DEFAULT NULL,
  `facebook_gender` varchar(10) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ユーザFacebookテーブル';

-- ----------------------------
-- Records of user_facebook_informations
-- ----------------------------
DROP TRIGGER IF EXISTS `before_insert_user_facebook_informations`;
DELIMITER ;;
CREATE TRIGGER `before_insert_user_facebook_informations` BEFORE INSERT ON `user_facebook_informations` FOR EACH ROW SET 
	new.created = UNIX_TIMESTAMP(),
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `before_update_user_facebook_informations`;
DELIMITER ;;
CREATE TRIGGER `before_update_user_facebook_informations` BEFORE UPDATE ON `user_facebook_informations` FOR EACH ROW SET 
	new.updated = UNIX_TIMESTAMP()
;;
DELIMITER ;
