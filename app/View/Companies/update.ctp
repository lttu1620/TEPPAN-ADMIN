<div class="row">
    <?php if (!empty($profileTab)) : ?>
        <section class="col-lg-12  ui-sortable">   
            <!-- Custom tabs (Charts with tabs)-->
            <div class="nav-tabs-custom" style="cursor: move;">
                <!-- Tabs within a box -->
                <?php echo $profileTab; ?>
            </div>
        </section>
    <?php endif ?>
    <div class="col-md-6">    
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('Common information company') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <?php
                echo $this->SimpleForm->render($commonForm);
                ?>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($introWhatForm)) { ?>
    <div class="row">
        <div class="col-md-6">    
            <div class="box box-primary">  
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo __('Introduction what') ?></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                    </div>
                </div>
                <div class="box-body">                
                    <?php
                    echo $this->SimpleForm->render($introWhatForm);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if (!empty($introWhyForm)) { ?>
    <div class="row">
        <div class="col-md-6">    
            <div class="box box-primary"> 
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo __('Introduction why') ?></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                    </div>
                </div>
                <div class="box-body">                
                    <?php
                    echo $this->SimpleForm->render($introWhyForm);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if (!empty($introHowForm)) { ?>
    <div class="row">
        <div class="col-md-6">    
            <div class="box box-primary">  
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo __('Introduction how') ?></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                    </div>
                </div>
                <div class="box-body">                
                    <?php
                    echo $this->SimpleForm->render($introHowForm);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if (!empty($introlikethisForm)) { ?>
    <div class="row">
        <div class="col-md-6">    
            <div class="box box-primary">  
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo __('Introduction likethis') ?></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                    </div>
                </div>
                <div class="box-body">                
                    <?php
                    echo $this->SimpleForm->render($introlikethisForm);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if (!empty($otherForm)) { ?>
    <div class="row">
        <div class="col-md-6">    
            <div class="box box-primary">  
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo __('Other information') ?></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                    </div>
                </div>
                <div class="box-body">                
                    <?php
                    echo $this->SimpleForm->render($otherForm);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
}?>