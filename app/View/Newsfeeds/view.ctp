<!-- Main row -->
<div class="row">
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-12">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title alignC textBlue">
                    <?php echo!empty($feed_info['title']) ? $feed_info['title'] : ''; ?>
                </h3>
            </div>
            <div class="box-body rssnews-feeds clearfix">
                <!-- RSS News Feeds -->
                <div class="news-feed-detail">
                    <div class="row top-img">
                        <div class="pull-left mT5">
                            <span class="date-time"><i class="fa fa-calendar"></i>
                                <?php echo !empty($feed_info['distribution_date']) ? $this->common->dateFormat($feed_info['distribution_date']) : ''; ?>
                            </span>
                            <span class="press"><i class="fa fa-book"></i>
                                <?php echo !empty($feed_info['site_name']) ? $feed_info['site_name'] : '' ?>
                            </span>
                        </div>
                        <div class="pull-right comment-like-favourite">
                            <span class="co-comments"><i class="fa fa-comment-o"></i>
                                <?php echo !empty($feed_info['comments_count']) ? $feed_info['comments_count'] : 0 ?>
                            </span>
                            <span class="co-likes"><i class="fa fa-likes"></i>
                                <?php echo !empty($feed_info['likes_count']) ? $feed_info['likes_count'] : 0 ?>
                            </span>
                            <span class="co-favourites"><i class="fa fa-favourites"></i>
                                <?php echo !empty($feed_info['favorite_count']) ? $feed_info['favorite_count'] : 0 ?>
                            </span>
                        </div>
                    </div>
                    <div class="imgs"><img src="<?php echo!empty($feed_info['image_url']) ? $feed_info['image_url'] : '' ?>" alt=""></div>
                    <div class="news-feeds-preview-detail">
                        <p><?php echo !empty($feed_info['short_content']) ? $feed_info['short_content'] : '' ?>...
                            <a target="_blank" href="<?php echo!empty($feed_info['detail_url']) ? $feed_info['detail_url'] : '' ?>" class="viewmore">続きを読む</a>
                        </p>
                    </div>

                    <!-- Chat box -->
                    <div class="box-success">
                        <div class="box-body chat full" id="chat-box">
                            <!-- chat item -->
                            <?php if (!empty($comments)): ?>
                                <?php foreach ($comments as $cm): ?>
                                    <div class="item news-feed-item-detail">
                                        <img src="<?php                                        
                                        $image = $cm['display_image'];
                                        if ($cm['is_company']) {
                                            $imageType = 'company';
                                        } else {
                                            $imageType = 'user';
                                        }
                                        echo $this->Common->thumb($image, '60x60', $imageType);?>" alt="user image" class="online"/>
                                        <span class="co-likes">
                                            <i class="fa fa-likes"></i>
                                            <?php echo $cm['like_count'] ?>
                                        </span>
                                        <p class="message">
                                            <a href="#" class="name">
                                                <?php echo $cm['name'] ?>
                                            </a>
                                            <span class="comment"><?php echo $cm['comment'] ?></span>
                                            <span class="date"><?php echo $this->Common->dateFormat($cm['created'])?></span>
                                        </p>
                                    </div><!-- /.item -->
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div><!-- /.chat -->
                    </div><!-- /.box (chat box) -->
                    <!-- paginations -->
                    <div class="clearfix">
                        <?php
                        echo $this->Paginate->render($total, $limit);
                        ?>                
                    </div>
                    <!-- /.paginations -->
                    <?php if($is_admin == 0): //just comment if not admin?>
                    <!-- input comments -->
                    <form id="frm_feedcomment" method="post" name="frm_feedcomment" role="form" action="<?php echo $this->request->here;?>">
                        <div class="form-group aLeft">
                            <label>コメント</label>
                            <textarea id="txt_comment" name="txt_comment" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                        <div class="form-group aRight">
                            <button type="submit" class="btn btn-primary"><?php echo __('Submit') ?></button>
                        </div>
                    </form>
                    <!-- /.input comments -->
                    <?php endif;?>
                </div>
                <!-- /.RSS News Feeds -->
            </div>
        </div>
    </section><!-- right col -->
</div><!-- /.row (main row) -->


