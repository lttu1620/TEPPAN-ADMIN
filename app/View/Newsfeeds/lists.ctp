<div class="row">
    <section class="col-md-3">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">編集部おすすめ</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <ul class="list-group">
                    <a href="<?php echo($this->Html->url('/newsfeeds/lists?tag_id=all')) ?>" class="list-group-item <?php if (isset($this->request->query['tag_id']) && $this->request->query['tag_id'] == 'all') echo 'active'; ?>">
                        <i class="fa fa-angle-double-right"></i>
                        <span class="badge bg-capture-red"></span>
                        <?php echo __('ALL') ?>
                    </a>
                    <?php foreach ($tagList as $tag): ?>
                        <?php if (!empty($tag['feed_count'])): ?>
                            <a href="<?php echo($this->Html->url('/newsfeeds/lists?tag_id=' . $tag['id'])) ?>" class="list-group-item <?php if (isset($this->request->query['tag_id']) && $this->request->query['tag_id'] == $tag['id']) echo 'active'; ?>">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="badge bg-capture-red"><?php echo ($tag['feed_count'] >= 1000) ? '999+' : $tag['feed_count'] ?></span>
                                <?php echo $this->common->truncate($tag['name'], 10) ?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div><!-- /.box-body -->
        </div>
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">媒体RSS</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <ul class="list-group">

                    <a href="<?php echo($this->Html->url('/newsfeeds')) ?>" class="list-group-item <?php if (empty($this->request->query['news_site_id'])) echo 'active'; ?>">
                        <span class="badge bg-capture-red"></span><i class="fa fa-angle-double-right"></i> <?php echo __('All') ?></a>

                    <?php if (!empty($sitelist)): ?>
                    <?php foreach ($sitelist as $site): ?>
                        <?php if (!empty($site['feed_count'])): ?>
                            <a href="<?php echo($this->Html->url('/newsfeeds/lists?news_site_id=' . $site['id'])) ?>" class="list-group-item <?php if (isset($this->request->query['news_site_id']) && $this->request->query['news_site_id'] == $site['id']) echo 'active'; ?>">
                                <i class="fa fa-angle-double-right"></i>
                                <span class="badge bg-capture-red"><?php echo ($site['feed_count'] >= 1000) ? '999+' : $site['feed_count'] ?></span>
                                <?php echo $this->common->truncate($site['name'], 10) ?></a>                
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php endif; ?>




            </div><!-- /.box-body -->
        </div>
    </section>

    <section class="col-md-9">
        <div class="box">
            <div class="box-header ui-sortable-handle">
                <h3 class="box-title">ニュース一覧 </h3>
                <div class="box-tools">
                    <?php
                    echo $this->Form->Create("Search", array('type' => 'get'));
                    ?>
                    <div class="input-group">
                        <?php
                        Echo $this->Form->input('title', array(
                            'class' => 'form-control input-sm pull-right',
                            'div' => FALSE,
                            'label' => FALSE,
                            'placeholder' => __('Search'),
                            'style' => 'width: 150px;height:30px',
                            'value' => $title
                        ));
                        ?>
                        <div class="input-group-btn"> 
                            <?php
                            echo $this->Form->button("<i class=\"fa fa-search\" ></i>", array(
                                'type' => 'submit',
                                'id' => 'btnSearch',
                                'class' => 'btn btn-sm btn-default',
                                'escape' => false));
                            ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body rssnews-feeds clearfix">              
                <!-- RSS News Feeds -->
                <?php foreach ($data as $item) : ?>
                    <div class="row news-feed-items">
                        <div class="news-feeds-image">
                            <div class="imgs">
                                <!--                                <a href="javascript:void(0);">-->
                                <?php
                                Echo $this->Html->image($item['image_url'], array(
                                    "alt" => $item['site_name'],
                                    'url' => array('controller' => 'newsfeeds', 'action' => 'view', $item['id'])
                                ));
                                ?>
                                <!--                                </a>-->
                            </div>
                            <div class="comment-like-favourite"> 
                                <a href="<?php echo($this->Html->url('/newsfeeds/view/' . $item['id'])) ?>">
                                    <span class="co-comments">
                                            <i class="fa fa-comment-o"></i>
                                            <?php
                                            echo $item['comments_count'];?>
                                    </span>
                                </a>

                                <a href="<?php echo($this->Html->url('/newsfeeds/like/' . $item['id'])) ?>" class="co-likes">
                                        <i class="fa fa-likes"></i>
                                        <?php echo $item['likes_count'];?>
                                </a>
                                <a href="<?php echo($this->Html->url('/newsfeeds/favorite/' . $item['id'])) ?>" class="co-favourites">
                                        <i class="fa fa-favourites"></i>
                                        <?php echo $item['favorite_count'];?>
                                </a>
                            </div>
                        </div>
                        <div class="preview-content">
                            <div class="preview-content-inner">
                                <?php
                                echo $this->Html->link($item['title'], array(
                                    'controller' => 'newsfeeds',
                                    'action' => 'view',
                                    $item['id']
                                    ), array(
                                    'class' => 'linkDetail'
                                ));
                                ?> 
                                <div class="bottom-nf-title">
                                    <span class="date-time"><i class="fa fa-calendar"></i>
                                        <?php
                                        //echo $this->Time->format($item['distribution_date'], '%Y.%m.%e %H:%M'); 
                                        echo $this->Common->dateFormat($item['distribution_date']);
                                        ?> 
                                    </span>
                                    <span class="press"><i class="fa fa-book"></i>
                                        <?php Echo$item['site_name'] ?>
                                    </span>
                                </div>
                                <table class=" table news-feeds-preview">
                                    <tr>
                                        <td>
                                            <div class="shortContent">
                                                <?php
                                                echo $item['short_content']
                                                ?>
                                            </div>
                                            <?php
                                            echo $this->Html->link('続きを読む', array(
                                                'controller' => 'newsfeeds',
                                                'action' => 'view',
                                                $item['id']
                                                ), array(
                                                'class' => 'viewmore Hidden',
                                            ));
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="news-feeds-bottom">
                                <table>
                                    <tbody><tr>
                                            <td >
                                                <span class="avatars-cmts"> 
                                                    <span class="countusercmts">
                                                        <?php
                                                        $userCount = $item['listUser'][0];
                                                        echo $userCount . ' 人のユーザがコメントしています。';
                                                        ?>
                                                    </span>                                                   
                                                    <?php
                                                    $lstUser = $item['listUser'][1];
                                                    foreach ($lstUser as $user) {
                                                        echo $this->Html->image($user['display_image'], array(
                                                            "title" => $user['name'],
                                                        ));
                                                        echo '&nbsp;';
                                                    }
                                                    ?>
                                                </span>
                                            </td>
                                            <td width="87">

                                                <?php
                                                echo $this->Form->button("<i class = \"fa fa-comment-o\" ></i> シェア", array(
                                                    'type' => 'submit',
                                                    'id' => 'btnShare',
                                                    'onclick' => "window.location.href='{$this->Html->Url("/newsfeeds/view/{$item['id']}")}';return false;",
                                                    'class' => 'btn btn-success',
                                                    'escape' => false));
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </div>                            
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php unset($item); ?>
                <!-- /.RSS News Feeds -->

            </div><!-- /.box-body -->
            <div class="box-footer clearfix">
                <?php
                echo $this->Paginate->render($total, $limit);
                ?>                
            </div>
        </div><!-- /.box -->
    </section>
</div>

<script>
    viewMore();
    function viewMore() {
        $.each($('.shortContent'), function (_index, _obj) {
            var _height = $(this).height();
            if (_height > 38)
            {
                $(this).next().removeClass('Hidden');
                $(this).height(38).css({'max-height': '38px', 'overflow': 'hidden'});
            } else
            {
                $(this).attr('style', '');
                $(this).next().addClass('Hidden');
            }           
        });
    }
</script>
