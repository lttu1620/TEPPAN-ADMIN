<div class="mailbox row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3 col-sm-4">                        
                        <?php echo $profileTab; ?>                          
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="box box-primary">  
                            <div class="box-body"> 
                            <?php        
                                echo $this->SimpleForm->render($userForm);
                            ?>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>