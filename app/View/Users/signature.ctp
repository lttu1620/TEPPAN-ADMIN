<div class="mailbox row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <?php if(!empty($profileTab)): ?>
                        <div class="col-md-3 col-sm-4">
                            <?php echo $profileTab; ?>
                        </div>
                    <?php endif ?>
                    <div class="col-md-9 col-sm-8">
                        <div class="box">
                            <?php
                            echo $this->SimpleTable->render($table);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>