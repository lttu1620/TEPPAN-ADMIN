<div class="row">
    <section class="col-lg-12  ui-sortable">  
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom" style="cursor: move;">
            <!-- Tabs within a box -->            
            <?php if (!empty($profileTab)) : ?>
                <?php echo $profileTab; ?>  
            <?php endif ?>
        </div>
    </section>

     <div class="col-md-6">    
        <div class="box box-primary">  
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo __('User profile') ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div> 
            <div class="box-body">                
                <?php
                    echo $this->SimpleForm->render($updateForm);
                ?>
            </div>
        </div>
    </div>
</div>