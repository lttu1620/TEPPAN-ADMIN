<div class="ui-widget">
    <label for="birds">Birds: </label>
    <input id="birds">
</div> 
<div class="ui-widget" style="margin-top:2em; font-family:Arial">
    Result:
    <div id="log" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content"></div>
</div>
<script>
    callbackF = function(item) {
        alert(item.id);
    }
    autocomplete = function(id, url, func) {
        $("#"+id).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: url,
                    dataType: "jsonp",
                    data: {
                        q: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                func(ui.item);               
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function() {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    }
    
    $(function() {      
        autocomplete('birds', "/ajax/autocomplete", callbackF);
        
        /*
        $("#birds").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/ajax/autocomplete",
                    dataType: "jsonp",
                    data: {
                        q: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                log(ui.item ?
                        "Selected: " + ui.item.id + ' : ' + ui.item.label :
                        "Nothing selected, input was " + this.value);
            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function() {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
        */
    });
</script>