
<div class="row">
    <div class="col-xs-120">  
        <div class="box box-primary">  
            <div data-original-title="Header tooltip" title="" data-toggle="tooltip" class="box-header">
                <h3 class="box-title"><?php echo __('Search') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-primary btn-xs search-collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div>  
            <div class="box-body search-body">          
                <?php
                echo $this->SimpleForm->render($searchForm);
                ?>                              
            </div>   
        </div>
    </div>
</div>
<!-- Main row -->
<div class="row">
    <div class="col-xs-120">   
        <div class="box box-primary">  
            <div class="box-body">                
                <div id="newscommentLike" style="min-height:500px;"><?php echo $newscommentLike; ?> </div>
            </div>
        </div>
    </div>
</div>

