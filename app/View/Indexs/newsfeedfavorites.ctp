<div class="row">
    <div class="col-xs-120">  
        <div class="box box-primary">  
            <div data-original-title="Header tooltip" title="" data-toggle="tooltip" class="box-header">
                <h3 class="box-title"><?php echo __('Search') ?></h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-primary btn-xs search-collapse"><i class="fa fa-minus"></i></button>                
                </div>
            </div>  
            <div class="box-body search-body">          
                <?php
                echo $this->SimpleForm->render($searchForm);
                ?>                              
            </div>   
        </div>
    </div>
</div>
<!-- Main row -->
<div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">               
                <li class="active"><a href="#chart-result" data-toggle="tab" id="tabChart">Line Chart</a></li>
            </ul>
            <div class=" no-padding">                         
                <div class="chart" id="chart" style="min-height:500px;"></div>
            </div>
        </div>
    </section>
</div>
<?php echo $chart ?> 