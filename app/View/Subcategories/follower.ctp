<div class="row">
    <div class="col-xs-120">    
        <div class="box box-primary">            
            <div class="box-body">                 
                <?php    
                    echo $this->SimpleForm->render($searchForm);                    
                ?>                
            </div>   
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
        <?php        
            echo $this->SimpleTable->render($table);
            echo $this->Paginate->render($total, $limit);   
        ?>
        </div>
    </div>
</div>