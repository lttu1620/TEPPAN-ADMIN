<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li <?php if ($controller == 'pages' && $action == 'index') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/'))?>">
                    <i class="fa fa-dashboard"></i> <span>ダッシュボード</span>
                </a>
            </li>
            <li <?php if ($controller == 'pages' && $action == 'dau') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/pages/dau'))?>">
                    <i class="fa fa-bar-chart-o"></i> <span>DAU</span>
                </a>
            </li>            
            <li class="treeview <?php if (($controller == 'admins' && $action != 'password') || $controller=='users') echo 'active' ?>">
                <a href="#">
                    <i class="fa fa-users"></i> <span><?php echo __('User Manager') ?></span><i
                        class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'admins' && $action != 'password') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/admins')) ?>"><i class="fa fa-wrench"></i>
                            <span><?php echo __('Admin Manager') ?></span></a>
                    </li>
                    <li<?php if ($controller == 'users') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/users')) ?>"><i class="fa fa-user"></i>
                            <span><?php echo __('User Manager') ?></span></a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php if ($controller=='settings' || ($controller == 'admins' && $action == 'password')) echo "active"?>">
                <a href="#">
                    <i class="fa fa-gear"></i> <span><?php echo __('Settings')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if (isset($this->request->query['type']) && $this->request->query['type'] == 'global') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=global'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Global settings')?></a>
                    </li>
                    <li<?php if (isset($this->request->query['type']) && $this->request->query['type'] == 'message') echo " class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/settings?type=message'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Message settings')?></a>
                    </li>
                    <li<?php if ($controller == 'admins' && $action == 'password') echo " class=\"active\""?>>
                        <a href="<?php echo $this->Html->Url("/admins/password") ?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Change password'); ?></a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php if (  in_array($controller, array(
                                                'templateopenlogs',
                                                'templatesendlogs'))
                                ) echo "active"?>">
                <a href="#">
                    <i class="fa fa-files-o"></i> <span><?php echo __('Logs')?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller=='templateopenlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/templateopenlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Open message logs')?></a></li>
                    <li<?php if ($controller=='templatesendlogs') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/templatesendlogs'))?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Send message logs')?></a></li>
                </ul>
            </li>
            <li <?php if ($controller == 'businesscards') echo "class=\"active\""?>>
                <a href="<?php echo($this->Html->url('/businesscards'))?>">
                    <i class="fa fa-envelope-o"></i> <span><?php echo __('Business Card')?></span>
                </a>
            </li>
            <li class="treeview <?php if ($controller == 'templates' || $controller == 'mailsituations' || $controller == 'mailtypes') echo 'active' ?>">
                <a href="#">
                    <i class="fa fa-inbox"></i> <span><?php echo __('Mail') ?></span><i
                        class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($controller == 'templates') echo "class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/templates')) ?>"><i
                                class="fa fa-angle-double-right"></i> <?php echo __('Mail Template') ?></a></li>
                    <li <?php if ($controller == 'mailsituations') echo "class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/mailsituations')) ?>"><i
                                class="fa fa-angle-double-right"></i> <?php echo __('Mail Situation') ?></a></li>
                    <li <?php if ($controller == 'mailtypes') echo "class=\"active\""?>>
                        <a href="<?php echo($this->Html->url('/mailtypes')) ?>"><i
                                class="fa fa-angle-double-right"></i> <?php echo __('Mail Type') ?></a></li>
                </ul>
            </li>
            <li class="treeview <?php if ($controller == 'universities' || $controller == 'campuses' || $controller == 'departments' || $controller == 'helpcontents') echo 'active' ?>">
                <a href="#">
                    <i class="fa fa-gears"></i> <span><?php echo __('Master') ?></span><i
                        class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($controller == 'universities')
                        echo " class=\"active\"" ?>><a href="<?php echo($this->Html->url('/universities')) ?>"><i
                                class="fa fa-angle-double-right"></i> <?php echo __('Universities') ?></a></li>
                    <li<?php if ($controller == 'campuses')
                        echo " class=\"active\"" ?>><a href="<?php echo($this->Html->url('/campuses')) ?>"><i
                                class="fa fa-angle-double-right"></i> <?php echo __('Campuses') ?></a></li>
                    <li<?php if ($controller == 'departments')
                        echo " class=\"active\"" ?>><a href="<?php echo($this->Html->url('/departments')) ?>"><i
                                class="fa fa-angle-double-right"></i> <?php echo __('Departments') ?></a></li>
                    <li<?php if ($controller=='helpcontents') echo " class=\"active\""?>><a href="<?php echo($this->Html->url('/helpcontents'))?>"><i class="fa fa-angle-double-right"></i> <span><?php echo __('Help')?></span></a></li>
                </ul>
            </li>
            <li class="treeview <?php if ($controller=='system') echo "active"?>">
                <a href="#">
                    <i class="fa fa-wrench"></i> <span><?php echo __('System') ?></span><i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li<?php if ($action=='deletecache') echo " class=\"active\""?>><a href="<?php echo $this->Html->url('/system/deletecache')?>"><i class="fa fa-angle-double-right"></i> <?php echo __('Delete cache')?></a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
