<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $meta['title'] . '｜Mail Template ';?></title>
    <link href='css/lp.css' rel='stylesheet' type='text/css'>
    <link href='css/custom.css' rel='stylesheet' type='text/css'>
    <link href="images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">

    <meta property="og:site_name" content="<?php echo $meta['title'];?>" />
    <meta property="og:title" content="<?php echo $meta['title'];?>" />
    <meta property="og:image" content="images/teppan-ogp.png" />
    <meta property="og:description" content="就職活動のメールコミュニケーションを礼儀正しく、効率的に。就活生を応援する便利な就活メールテンプレートの「テッパン」アプリが登場！" />
    <meta property="og:url" content="http://teppan.email/lp" />
    <meta property="og:type" content="website" />

    <meta name="description" content="就職活動のメールコミュニケーションを礼儀正しく、効率的に。就活生を応援する便利な就活メールテンプレートの「テッパン」アプリが登場！インターシップや、採用面接、OB・OG訪問などの際に、礼儀正しいメールをしっかりと送信できる手助けがいっぱい詰まっています。" />
    <meta name="keywords" content="就活,メールテンプレート,Teppan,就職活動,テンプレ,鉄板" />
</head>
<body id="top">
	<div id="pink_Wrapper" >
		<div id="wrapper">
		    <?php echo $this->fetch('content'); ?>
		</div>
	</div>
</body>
</html>
