<section class="col-md-12">
    <div class="row">        
        <div class="box-body">                
            <div class="form-body">
                <img src="<?php echo $imageUrl ?>" id="image" style="border:solid 1px;">                                     
                <div class="form-group button-group">
                    <div class="submit">
                        <input tabindex="0" id="crop" type="button" class="btn btn-primary pull-left" value="保存">
                    </div>
                </div>             
                <div class="cls"></div>                                         
            </div>
        </div>        
    </div>        
</section>
<script>

    $(document).ready(function () {
        var pic_real_width = 1, pic_real_height = 1, pic_width = 1, pic_height = 1;
        $("<img/>")  // Make in memory copy of image to avoid css issues
                .attr("src", $("#image").attr("src"))
                .load(function () {
                    pic_real_width = this.width; // Note: $(this).width() will not
                    pic_real_height = this.height; // work for in memory images.
                });
        setTimeout(function () {
            pic_width = $("#image").width();
            pic_height = $("#image").height();
            console.log("actual: " + pic_real_width + "--" + pic_real_height + "|| web image: " + pic_width + "--" + pic_height);
        }, 200);

        var info = "<?php echo $info ?>";
        var x1 = 0;
        var y1 = 0;
        var x2 = 0;
        var y2 = 0;
        var width = 0;
        var height = 0;

        $('#image').imgAreaSelect({
            handles: true,
            instance: true,
            enable: true,
            onSelectEnd: function (img, selection) {
                x1 = selection.x1;
                y1 = selection.y1;
                x2 = selection.x2;
                y2 = selection.y2;
                width = selection.width;
                height = selection.height
            }
            // aspectRatio: '1:1'
        });
        $("#crop").click(function () {
            if (width == 0 && height == 0) {
                alert("<?php echo __('Please choose image area to crop'); ?>");
                return false;
            }
            var data = {
                info: info,
                x1: pic_real_width > pic_width ? (x1 * pic_real_width / pic_width) : x1,
                y1: pic_real_height > pic_height ? (y1 * pic_real_height / pic_height) : y1,
                x2: pic_real_width > pic_width ? (x2 * pic_real_width / pic_width) : x2,
                y2: pic_real_height > pic_height ? (y2 * pic_real_height / pic_height) : y2
            };
            $.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: baseUrl + '/images/crop',
                data: data,
                dataType: 'jsonp',
                jsonp: 'callback',
                success: function (response) {
                    if (response['status'] == '200') {
                        var _imgsrc = response['message'];
                        var _element = response['element'];
                        // Call method from parent
                        window.parent.closeDiaglog(_imgsrc, _element);
                    } else {
                        alert(response['message']);
                    }
                }
            });
            return false;
        });
    });
</script>