<div class="header"><?php echo __('Register company')?></div>
    <div class="body bg-gray">        
        <p>        
        <?php if ($AppUI->company_id > 0 && $AppUI->recruiter_admin == 0 && $AppUI->is_approved == 0) : ?>
            あなたが企業を登録しました。 <strong><?php echo $AppUI->company_name;?></strong>, その企業のアドミンからの認証をお待ちください。<br/>
            <center>
                <a href="#" onclick="<?php echo $resendLink?>" class="resendUrl">企業のアドミンへの認証用メールを再度送りました。</a><br/>                
            </center>
        <br/>
        <?php endif; ?>
            
        <?php if (!empty($updateForm['elements'])) : ?>
        <p><strong>あなたが所属する会社の名前を登録してください。</strong></p>
        <?php
            echo $this->SimpleForm->render($updateForm);           
		?>        
        </p>
        <?php endif; ?>
    </div>
</div>
