<!---- Left ---->
<div class="secLeft">
    <img src="images/logo.png" width="430" height="326" />
    <div class="tabAll">
        <ul>
            <li><a id="id01" class="top tab01 active" href="#" onclick="return false;">tab01</a></li>
            <li><a id="id02" class="top tab02" href="#" onclick="return false;">tab02</a></li>
            <li><a id="id03" class="top tab03" href="#" onclick="return false;">tab03</a></li>
            <li><a id="id04" class="top tab04" href="#" onclick="return false;">tab04</a></li>
        </ul>
    </div>
</div>
<!---- Right ---->
<div class="secRight" style="overflow:hidden">
    <div class="social">
        <div class="social_item">
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://teppan.email" data-text="就職活動のメールコミュニケーションを礼儀正しく効率的に。 TEPPAN 就活メールテンプレート |" data-via="shukatsulab" data-lang="ja" data-related="shukatsulab">ツイート</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        </div>
        <div class="social_item">
            <iframe src="//www.facebook.com/plugins/like.php?href=https://www.facebook.com/shukatsulab%3Ffref%3Dts&amp;width&amp;height=336&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=674403482679977&layout=button_count" scrolling="no" frameborder="0" style="overflow:hidden; height:22px; width:100%;" allowTransparency="true"></iframe>
        </div>
    </div>
    <div class="appStore">
        <?php if(Configure::read('AppStore.Url')['ios'] =='#'){ ?>
            <a target="" href="<?php echo Configure::read('AppStore.Url')['ios'] ?>" title="公開予定"><img src="images/app-store.png" width="184" height="54" alt="apple store" style="opacity: 0.5;" alt="公開予定" title="公開予定" /></a>
        <?php }else{ ?>
            <a target="_blank" href="<?php echo Configure::read('AppStore.Url')['ios'] ?>"><img src="images/app-store.png" width="184" height="54" alt="apple store" /></a> 
        <?php } ?> 
        <a target="_blank" href="<?php echo Configure::read('AppStore.Url')['android'] ?>"><img src="images/app_google-play.png" width="184" height="54" alt="google play" /></a>
    </div>
    <div class="producted"><img src="images/produced.png" width="331" height="136" alt="#" /></div>
    <!---- Tab 01 ---->
    <div id="tab01" class="tabContents">
        <div class="imgMain">
            <img src="images/screen_iphone_01.png" width="286" height="505" alt="#" />
        </div>
        <div class="tab01Right">
            <img class="imgText01" src="images/text_01.png" width="343" height="112" alt="#" />
            <ul>
                <li class="item01"><a href="#">item01</a></li>
                <li class="item02"><a href="#">item02</a></li>
                <li class="item03"><a href="#">item03</a></li>
                <li class="item04"><a href="#">item04</a></li>
                <li class="item05"><a href="#">item05</a></li>
                <li class="item06"><a href="#">item06</a></li>
                <li class="item07"><a href="#">item07</a></li>
                <li class="item08"><a href="#">item08</a></li>
                <li class="item09"><a href="#">item09</a></li>
                <li class="item10"><a href="#">item10</a></li>
                <li class="item11"><a href="#">item11</a></li>
                <li class="item12"><a href="#">item12</a></li>
                <br class="clear" />
            </ul>
            <img class="imgText02" src="images/text_02.png" width="355" height="24" alt="#" />
        </div>
    </div>
    <!---- Tab 02 ---->
    <div id="tab02" class="tabContents">
        <div class="imgMain">
            <img src="images/screen_iphone_02.png" width="286" height="505" alt="#" />
        </div>
        <div class="tab02Right">
            <img class="imgText03" src="images/text_03.png" width="388" height="114" alt="#" />
            <div class="content">
                <ul>
                    <li class="japan"><a href="#">japan</a></li>
                    <li class="english"><a href="#">english</a></li>
                    <li class="china"><a href="#">china</a></li>
                </ul>
            </div>
            <img class="imgText04" src="images/text_04.png" width="385" height="79" alt="#" />
        </div>
    </div>
    <!---- Tab 03 ---->
    <div id="tab03" class="tabContents">
        <div class="imgMain">
            <img src="images/screen_iphone_03.png" width="286" height="505" alt="#" />
        </div>
        <div class="tab03Right">
            <img src="images/text_05.png" width="359" height="151" alt="#" />
            <div class="content"><img src="images/img_tab03.png" width="359" height="426" alt="#" /></div>
            <img src="images/text_06.png" width="359" height="29" alt="#" />
        </div>
    </div>
    <!---- Tab 04 ---->
    <div id="tab04" class="tabContents">
        <div class="imgMain">
            <img class="imgText05" src="images/screen_iphone_04.png" width="286" height="505" alt="#" />
        </div>
        <div class="tab04Right">
            <img class="imgText06" src="images/img_tab04.png" width="341" height="672" alt="#" />
        </div>
    </div>
</div>
<!---- Bottom ---->
<div class="secBottom">
    <?php echo $this->render('/Pages/lp-news','ajax') ?>
    <div class="facebook">
        <div class="fb_like_box">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/shukatsulab%3Ffref%3Dts&amp;width=388&amp;height=325&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=674403482679977" scrolling="no" frameborder="0" style="overflow:hidden; width:100%; min-height:325px;" allowTransparency="true"></iframe>
        </div>
    </div>
</div>
<div class="copyright">Copyright © TEPPAN by Oceanize,Inc. All rights reserved.</div>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
    var flag = true;
    changeTab = function(index){
        var tab = $('.tabContents:eq('+index+')');
        $('.tabAll ul li a').removeClass("active");
        $('.'+$(tab).attr('id')).addClass("active");
        $(tab).fadeIn();
        if (flag==true) {
            v = setTimeout(function(){$(tab).fadeOut(500);},4600);
            if (index+1 < $('.tabContents').length){
                t = setTimeout("changeTab("+(index+1)+")",5000);
            } else {
                t = setTimeout("changeTab(0)",5000);
            }
        } else {
            clearTimeout(v);
        }
    }
    $(document).ready(function(){
        $(".tabContents").hide();
        $('.tabContents').mouseover(function(){
            clearTimeout(t);
            var index = $('.tabContents').index(this);
            flag = false;
            changeTab(index);
        }).mouseout(function(){
            var index = $('.tabContents').index(this);
            flag = true;
            changeTab(index);
        });
        changeTab(0);
        $('.tabAll ul li a').click(function(){
            var index = $('a').index(this);
            $(".tabContents").hide();
            clearTimeout(t);
            changeTab(index);
        });
    });
</script>
<?php echo Configure::read('Google.Analyticstracking') ?>