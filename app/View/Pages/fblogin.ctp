<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
$(function() {
    FB.init({
        appId: '<?php echo Configure::read('Facebook.appId')?>', 
        cookie: true, 
        status: true, 
        oauth: true,
        xfbml: true		
    });
    FB.login(function(response) {
       if (response.authResponse) {				
            FB.api('/me', function(response) {
                var params = response;
                var url = '/ajax/fblogin';
                $.ajax({
                    cache	: false,
                    async	: true,
                    data	: params,
                    url		: url,
                    success	: function(redirectUrl) {
						if (redirectUrl) {
							location.href = redirectUrl;
						}
                    }
                });				
            });
       } else {
            console.log('User cancelled login or did not fully authorize.');
       }
    }, {scope: 'email,user_likes'});
});
</script>
Login ...