<?php echo $this->Html->image('logo.png'); ?>
<br />
<div class="header"><?php echo __('Sign In') ?></div>

<div class="body bg-gray">
    <?php echo $this->SimpleForm->render($adminLoginForm);?>
</div>

<script src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
    $(function () {
        FB.init({
            appId: '<?php echo Configure::read('Facebook.appId') ?>',
            cookie: true,
            status: true,
            oauth: true,
            xfbml: true
        });
    });
</script>