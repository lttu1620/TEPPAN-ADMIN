<div id="section-bottom">
    <div class="section">
        <div class="apple fL">
            <a target="_blank" href="<?php echo Configure::read('AppStore.Url')['ios'] ?>"><img src="images/sp/bt_apple.png" alt="#" /></a>
        </div>
        <div class="googlePlay fR">
            <a target="_blank" href="<?php echo Configure::read('AppStore.Url')['android'] ?>"><img src="images/sp/bt_google_play.png" alt="#" /></a>
        </div>
        <br class="clear" />
    </div>
    <div class="section end">
        <div class="social fL">
            <div class="roll01">
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://teppan.email" data-text="就職活動のメールコミュニケーションを礼儀正しく効率的に。 TEPPAN 就活メールテンプレート |" data-via="shukatsulab" data-lang="ja" data-related="shukatsulab">ツイート</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                <br class="clear" />
            </div>
            <div class="roll02">
                <iframe src="//www.facebook.com/plugins/like.php?href=https://www.facebook.com/shukatsulab%3Ffref%3Dts&amp;width&amp;height=336&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=674403482679977&layout=button_count" scrolling="no" frameborder="0" style="overflow:hidden; height:22px; width:100%;" allowTransparency="true"></iframe>
                <br class="clear" />
            </div>
        </div>
        <div class="producted fR"><img src="images/sp/producted.png" alt="#" /></div>
        <br class="clear" />
    </div>
</div>