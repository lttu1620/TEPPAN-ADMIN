<div id="news">
    <div class="title">NEWS</div>

    <ul>
        <li>
            <span class="date-col">2015.04.13</span>
            <a>「TEPPAN」のiOSアプリをリリースしました。</a>
        </li>
        <li>
            <span class="date-col">2015.04.03</span>
            <a>「TEPPAN」のAndroidアプリをリリースしました。 </a>
        </li>
        <li>
            <span class="date-col">2015.04.01</span>
            <a>株式会社アクセスヒューマネクストと株式会社オーシャナイズが共同企画「TEPPAN」をスタートさせました。</a>
        </li>
    </ul>

</div>