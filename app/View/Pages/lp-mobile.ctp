<!---- Header ---->
<div id="header">
    <img src="images/sp/logo.png" alt="#" />
</div>
<!---- menu 01 ---->
<div id="mn_tab01" class="accordion" data-content="#content01">
    <img src="images/sp/tab01.png" alt="#" />
    <!---- content 01 ---->
    <div id="content01" class="content show">
        <div class="left">
            <img src="images/sp/iphone_01.png" alt="#" />
            <div class="bookmark"><img src="images/sp/bookmask.png" alt="#" /></div>
        </div>
        <div class="right">
            <ul>
                <li class="item01"><a href="#">item01</a></li>
                <li class="item02"><a href="#">item02</a></li>
                <li class="item03"><a href="#">item03</a></li>
                <li class="item04"><a href="#">item04</a></li>
                <li class="item05"><a href="#">item05</a></li>
                <li class="item06"><a href="#">item06</a></li>
                <li class="item07"><a href="#">item07</a></li>
                <li class="item08"><a href="#">item08</a></li>
                <li class="item09"><a href="#">item09</a></li>
                <li class="item10"><a href="#">item10</a></li>
                <li class="item11"><a href="#">item11</a></li>
                <li class="item12"><a href="#">item12</a></li>
                <br class="clear" />
            </ul>
        </div>
        <br class="clear" />
        <div class="section tBottom"><img src="images/sp/text_02.png" alt="#" /></div>
    <?php 
        echo $this->render('/Pages/lp-mobile-section-bottom','ajax');
        $this->hasRendered = false;
    ?>  
    </div>
</div>

<!---- menu 02 ---->
<div id="mn_tab02" class="accordion" data-content="#content02">
    <img src="images/sp/tab02.png" alt="#" />
    <!---- content 02 ---->
    <div id="content02" class="content">
        <div class="left">
            <img src="images/sp/iphone_02.png" alt="#" />
        </div>
        <div class="right">
            <ul>
                <li class="english"><a href="#"><img src="images/sp/ico_lang_english.png" alt="english" /></a></li>
                <li class="china"><a href="#"><img src="images/sp/ico_lang_china.png" alt="china" /></a></li>
                <li class="japan"><a href="#"><img src="images/sp/ico_lang_japan.png" alt="japan" /></a></li>
            </ul>
        </div>
        <br class="clear" />
        <div class="section tBottom"><img src="images/sp/text_04.png" alt="#" /></div>
    <?php 
        echo $this->render('/Pages/lp-mobile-section-bottom','ajax');
        $this->hasRendered = false;
    ?>
    </div>
</div>

<!---- menu 03 ---->
<div id="mn_tab03" class="accordion" data-content="#content03">
    <img src="images/sp/tab03.png" alt="#" />

    <!---- content 03 ---->
    <div id="content03" class="content">
        <div class="left">
            <img src="images/sp/iphone_03.png" alt="#" />
        </div>
        <div class="right">
            <img src="images/sp/img_tab03.png" alt="#" />
        </div>
        <br class="clear" />
        <div class="section tBottom"><img src="images/sp/text_06.png" alt="#" /></div>   
    <?php 
        echo $this->render('/Pages/lp-mobile-section-bottom','ajax');
        $this->hasRendered = false;
    ?>
    </div>
</div>

<!---- menu 04 ---->
<div id="mn_tab04" class="accordion"  data-content="#content04">
    <img src="images/sp/tab04.png" alt="#" />
    <!---- content 04 ---->
    <div id="content04" class="content">
        <div class="left">
            <img src="images/sp/iphone_04.png" alt="#" />
        </div>
        <div class="right">
            <img src="images/sp/img_tab04.png" alt="#" />
        </div>
        <br class="clear" />
        <div class="section tBottom"><img src="images/sp/text_08.png" alt="#" /></div>
    <?php 
        echo $this->render('/Pages/lp-mobile-section-bottom','ajax');
        $this->hasRendered = false;
    ?>
    </div>
</div>

<!---- news ---->
<div id="news">
    <div class="section">
        <?php echo $this->render('/Pages/lp-news','ajax') ?>
    </div>
    <div class="section">
        <div class="apple fL">
            <a target="_blank" href="<?php echo Configure::read('AppStore.Url')['ios'] ?>"><img src="images/sp/bt_apple.png" alt="#" /></a>
        </div>
        <div class="googlePlay fR">
            <a target="_blank" href="<?php echo Configure::read('AppStore.Url')['android'] ?>"><img src="images/sp/bt_google_play.png" alt="#" /></a>
        </div>
        <br class="clear" />
    </div>
    <div class="section facebook">
        <div class="fb_like_box">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/shukatsulab%3Ffref%3Dts&amp;width&amp;height=336&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=674403482679977" scrolling="no" frameborder="0" style="overflow:hidden; width:100%; min-height:336px; background: #ffffff;" allowTransparency="false"></iframe>
        </div>
    </div>
</div>
<!---- link to top ---->
<div id="linkTop">
    <div class="section"><a href="#"><img src="images/sp/ico_totop.png" alt="link to top" /></a></div>
</div>
<!---- Copyright ---->
<div class="copyright">Copyright © TEPPAN by Oceanize,Inc. All rights reserved.</div>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript">
    var duration = 400;
    $(document).ready(function () {
        $(".accordion").click(function (e) {

            var $content = $($(this).attr("data-content"));
            var $tab = $(this);
            if (!$content.hasClass("show")) {
                $(".content.show").animate({height: 'hide', opacity: 'hide'}, duration, function () {
                    $(this).removeClass("show");
                    $('html,body').animate({ scrollTop: $tab.offset().top }, 300, 'linear', function () {
                        $content.animate({height: 'show', opacity: 'show'}, duration, function () {
                            $content.addClass("show");
                        });
                    });
                });
            }
        });
    });
</script>
<?php echo Configure::read('Google.Analyticstracking') ?>