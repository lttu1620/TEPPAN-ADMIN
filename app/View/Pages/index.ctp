<!-- Main content -->
<h2>
	総会員数 <small>[<?php echo date('Y-m-d H:i'); ?>]
	</small>
</h2>
<ol class="breadcrumb">
	<li>
		<a href="#"><i class="fa fa-user"></i> 会員</a>
	</li>
	<li class="active">総会員数</li>
</ol>

<div class="row">
    <div class="col-lg-3 col-xs-12">
        <!-- users number -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    <?php echo $data['user_registed_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
                </h3>
                <p>メール登録会員数</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
            <div class="small-box-footer">アプリ合計数</div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <!-- users number -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    <?php echo $data['user_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
                </h3>
                <p>現在のアプリ会員数</p>
            </div>
            <div class="icon">
                <i class="ion ion-person"></i>
            </div>
            <div class="small-box-footer">アプリ合計数</div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- ios users number -->
        <div class="small-box bg-teal">
            <div class="inner">
                <h3>
                    <?php echo $data['user_ios_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
                </h3>
                <p>現在のiPhoneアプリ会員数</p>
            </div>
            <div class="icon">
                <i class="fa fa-apple"></i>
            </div>
            <div class="small-box-footer"><?php echo __('iPhone')?></div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- android users number -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>
                    <?php echo $data['user_android_count']?>&nbsp;<sup style="font-size: 20px">人</sup>
                </h3>
                <p>現在のAndroidアプリ会員数</p>
            </div>
            <div class="icon">
                <i class="fa fa-android"></i>
            </div>
            <div class="small-box-footer"><?php echo __('Android')?></div>
        </div>
    </div>
</div>

<div class="row">
    
    <div class="col-lg-3 col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">月間テンプレ利用数ランキング</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>テンプレ名</th>                      
                            <th style="width: 50px">合計</th>
                        </tr>
                        <?php foreach ($data['monthly_used_templates'] as $i => $item) : ?>
                        <tr>
                            <td><?= $i+ 1 ?></td>
                            <td><?= $item['title'] ?></td>                       
                            <td><span class="badge bg-red"><?= $item['count_used'] ?></span></td>
                        </tr>   
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->           
        </div><!-- /.box -->
    </div>
    
    <div class="col-lg-3 col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">テンプレお気に入り数ランキング</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>テンプレ名</th>                      
                            <th style="width: 50px">合計</th>
                        </tr>
                        <?php foreach ($data['favorited_templates'] as $i => $item) : ?>
                        <tr>
                            <td><?= $i+ 1 ?></td>
                            <td><?= $item['title'] ?></td>                       
                            <td><span class="badge bg-red"><?= $item['count_favorite'] ?></span></td>
                        </tr>   
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->           
        </div><!-- /.box -->
    </div>    
    
    <div class="col-lg-3 col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">総テンプレ利用数ランキング</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>テンプレ名</th>                      
                            <th style="width: 50px">合計</th>
                        </tr>
                        <?php foreach ($data['used_templates'] as $i => $item) : ?>
                        <tr>
                            <td><?= $i+ 1 ?></td>
                            <td><?= $item['title'] ?></td>                       
                            <td><span class="badge bg-red"><?= $item['count_used'] ?></span></td>
                        </tr>   
                        <?php endforeach; ?>
                    </tbody>
                </table
            </div><!-- /.box-body -->           
        </div><!-- /.box -->
    </div>  
    
</div>
