<?php

/**
 * Login's model.
 *
 * @package Model
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Login extends AppModel
{
	public $name = 'Login';
	public $table = 'admins';
	public $primaryKey = 'id';

    /**
     * Validate data when login.
     *
     * @author Truongnn
     * @param array $data Input data.
     * @return bool Returns the boolean.
     */
	public function validate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'login'    => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Login ID can not empty'),
				),
			),
			'password' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Password can not empty'),
				),
			),
		);
		if ($this->validates()) {
			return true;
		}
		return false;
	}

}
