<?php

/**
 * Mailsituation's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mailsituation extends AppModel
{
	public $name = 'Mailsituation';
	public $table = 'mail_situations';
	public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
	public function validateInsertUpdate($data)
	{
		$this->set($data[$this->name]);
		$this->validate = array(
			'name' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Name can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 1, 128),
					'message' => 'Between 1 to 128 characters'
				),
			),
			'name_en' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('English name can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 1, 128),
					'message' => 'Between 1 to 128 characters'
				),
			),
			'name_cn' => array(
				'notEmpty' => array(
					'rule'    => 'notEmpty',
					'message' => __('Chinese name can not empty'),
				),
				'between'  => array(
					'rule'    => array('between', 1, 128),
					'message' => 'Between 1 to 128 characters'
				),
			),
		);
		if ($this->validates()) {
			return true;
		}

		return false;
	}
}
