<?php

/**
 * Helpcontents of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class Helpcontent extends AppModel {

    public $name = 'Helpcontent';
    public $table = 'help_contents';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author Truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'title' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Title can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 40),
                    'message' => __('Between 2 to 40 characters')
                ),
            ),
        );
        if ($this->validates())
            return true;
        return false;
    }

}
