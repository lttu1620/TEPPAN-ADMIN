<?php

/**
 * Templatefavorite's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Templatefavorite extends AppModel
{
    public $name = 'Templatefavorite';
    public $table = 'template_favorites';
    public $primaryKey = 'id';
}