<?php

/**
 * Universitys of model.
 *
 * @package Model
 * @version 1.0
 * @author Truongnn
 * @copyright Oceanize INC
 */
class University extends AppModel {

    public $name = 'University';
    public $table = 'universities';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function validateInsertUpdate($data) {
        $this->set($data[$this->name]);
        $this->validate = array(
            'name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('Name can not empty'),
                ),
                'between' => array(
                    'rule' => array('between', 2, 45),
                    'message' => __('Between 2 to 120 characters')
                ),
            ),
            'kana' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => __('kana can not empty')
                ),
                'between' => array(
                    'rule' => array('between', 2, 90),
                    'message' => __('Between 2 to 90')
                )
            )
        );
        if ($this->validates())
            return true;
        return false;
    }

}
