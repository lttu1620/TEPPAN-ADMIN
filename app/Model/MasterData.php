<?php

App::uses('AppModel', 'Model');

/**
 * MasterData of model
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class MasterData extends AppModel {

    /**
     * Get all universities.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function universities_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('universities_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_universities_all'));
            AppCache::write(Configure::read('universities_all')->key, $result, Configure::read('universities_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get all universities include key and value.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function universities_all_key_value() {
        $result = AppCache::read(Configure::read('universities_all_key_value')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_universities_all'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'id', 'name');
            AppCache::write(Configure::read('universities_all_key_value')->key, $result, Configure::read('universities_all_key_value')->seconds);
        }
        return $result;
    }

    /**
     * Get all campuses.
     *
     * @author thailh
     * @param array $toKeyValue Input array.
     * @return array Returns the array.
     */
    public static function campuses_all($toKeyValue = array()) {
        $result = AppCache::read(Configure::read('campuses_all')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_campuses_all'));
            AppCache::write(Configure::read('campuses_all')->key, $result, Configure::read('campuses_all')->seconds);
        }
        if (count($toKeyValue) == 2) {
            $result = static::getCommonComponent()->arrayKeyValue($result, $toKeyValue[0], $toKeyValue[1]);
        }
        return $result;
    }

    /**
     * Get info global_settings.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function global_settings() {
        $result = AppCache::read(Configure::read('global_settings')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_settings_all'), array('type' => 'global'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'name', 'value');
            AppCache::write(Configure::read('global_settings')->key, $result, Configure::read('global_settings')->seconds);
        }
        return $result;
    }

    /**
     * Get all mail situation include key and value.
     *
     * @author Le Tuan Tu
     * @return array Returns the array.
     */
    public static function mail_situations_all_key_value() {
        $result = AppCache::read(Configure::read('mail_situations_all_key_value')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_mailsituations_all'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'id', 'name');
            AppCache::write(Configure::read('mail_situations_all_key_value')->key, $result, Configure::read('mail_situations_all_key_value')->seconds);
        }

        return $result;
    }

    /**
     * Get all mail type include key and value.
     *
     * @author Le Tuan Tu
     * @return array Returns the array.
     */
    public static function mail_types_all_key_value() {
        $result = AppCache::read(Configure::read('mail_types_all_key_value')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_mailtypes_all'));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'id', 'name');
            AppCache::write(Configure::read('mail_types_all_key_value')->key, $result, Configure::read('mail_types_all_key_value')->seconds);
        }

        return $result;
    }

    /**
     * Get all template include key and value.
     *
     * @author Le Tuan Tu
     * @return array Returns the array.
     */
    public static function templates_all_key_value() {
        $result = AppCache::read(Configure::read('templates_all_key_value')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_templates_all'), array('user_id' => 0));
            $result = static::getCommonComponent()->arrayKeyValue($result, 'id', 'title');
            AppCache::write(Configure::read('templates_all_key_value')->key, $result, Configure::read('templates_all_key_value')->seconds);
        }

        return $result;
    }
    
    /**
     * Get report genaral.
     *
     * @author thailh
     * @return array Returns the array.
     */
    public static function report_general() {
        $result = AppCache::read(Configure::read('report_general')->key);
        if ($result === false) {
            $result = Api::call(Configure::read('API.url_reports_general'), array());
            AppCache::write(Configure::read('report_general')->key, $result, Configure::read('report_general')->seconds);
        }
        return $result;
    }
}
