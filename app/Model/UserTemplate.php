<?php

/**
 * UserTemplate's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserTemplate extends AppModel
{
	public $name = 'UserTemplate';
	public $table = 'user_templates';
	public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
	public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'title'    => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Title can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 128),
                    'message' => 'Between 1 to 128 characters'
                ),
            )
        );
        if ($this->validates()) {
            return true;
        }

        return false;
    }

}
