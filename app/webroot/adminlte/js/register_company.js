var nameList = get_name_list();
var kanaList = get_kana_list();
$(document).ready(function() { 
    $('#name').focusout(function() { 
        search_company_name($(this).val());        
    });
    $('#kana').focusout(function() {
        search_company_kana($(this).val());         
    });    
});

function get_name_list() {
    var strnames = $('#name').attr('name-list');
    if(typeof (strnames) !== 'undefined'){
        var arr_name = to_array(JSON.parse(strnames));
        return arr_name;
    }
    return false;
}

function get_kana_list() {
    var strkanas = $('#kana').attr('kana-list');
    if(typeof (strkanas) !== 'undefined'){
        var arr_kana = to_array(JSON.parse(strkanas));
        return arr_kana;
    }
    return false;
}

function change_company_name(item) {
    search_company_name(item.value);
    $('#kana').focus();
}

function change_company_kana(item) {       
    search_company_kana(item.value);
    $('#name').focus();
}

function search_value(arr, val) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === val) { 			
            return i;
        }
    }
    return false;
}

function to_array(obj){
    var arr = [];
    $.each(obj, function(k, v){
        arr.push(v);
    });
    
    return arr;
}

function check_validate(){
    var name = $('name').val();
    var pos_name = search_value(arr_name, name);
    var kana = $('kana').val();
    var pos_kana = search_value(arr_kana, kana);
    
    if(name !== '' && kana!== ''){
        if((pos_name !== false) && (kana != arr_kana[pos_name])){
                return false;
        }
        if((pos_kana !== false) && (name != array_name[pos_kana])){
                return false;
        }
    }
    return true;
}

function search_company_name(value) { 
    if (value !== '') {
        var pos = search_value(nameList, value);
        if (pos !== false) {     			       
            $('#kana').val(kanaList[pos]);
            return pos;
        }
    }
    return false;
}

function search_company_kana(value) {    
    if (value !== '') {
        var pos = search_value(kanaList, value);
        if (pos !== false) {           
            $('#name').val(nameList[pos]);
            return pos;
        }
    }
    return false;
}

function submit_register_compamy() {
    var searchName = search_company_name($('#name').val());
    var searchKana = search_company_kana($('#kana').val());   
    if ((searchName != searchKana) && (searchName != false || searchKana != false))  {
        alert('企業名とカナは一致していません。');
        return false;
    }
    return true;  
}
