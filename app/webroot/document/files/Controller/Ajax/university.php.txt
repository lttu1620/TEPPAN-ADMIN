<?php

$modelName = $this->Ajax->name;
if (!empty($id)) {

    // Thuc hien call API get data compus and Department
    $htmlResult = array();
    $lisData = Api::call(Configure::read('API.url_campuses_list'), array('university_id' => $id));
    if (Api::getError()) {
        $htmlResult['Campus'] = "\'\'";
    } else {
        foreach ($lisData as $item) {
            $htmlResult['Campus'][] = array(
                'id' => $item['id'],
                'value' => $item['name']
            );
        }
    }
    $lisData = Api::call(Configure::read('API.url_departments_list'), array('university_id' => $id));
    if (Api::getError()) {
        $htmlResult['Department'] = "\'\'";
    } else {
        foreach ($lisData as $item) {
            $htmlResult['Department'][] = array(
                'id' => $item['id'],
                'value' => $item['name']
            );
        }
    }
    // End ==> return data
    echo json_encode($htmlResult);
}
