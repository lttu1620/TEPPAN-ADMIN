<?php
$this->Cookie->httpOnly = true;
if ($this->Auth->logout()) {
	$loginUrl = 'login';
    return $this->redirect('/'.$loginUrl);
}
