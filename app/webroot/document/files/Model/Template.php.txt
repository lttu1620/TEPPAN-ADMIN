<?php

/**
 * Template's model.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Template extends AppModel
{
    public $name = 'Template';
    public $table = 'templates';
    public $primaryKey = 'id';

    /**
     * Verify data before the processing to insert or update
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Returns the boolean
     */
    public function validateInsertUpdate($data)
    {
        $this->set($data[$this->name]);
        $this->validate = array(
            'title'    => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Title can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 128),
                    'message' => 'Between 1 to 128 characters'
                ),
            ),
            'title_en' => array(
                'between'  => array(
                    'rule'    => array('between', 0, 128),
                    'message' => 'Between 0 to 128 characters'
                ),
            ),
            'title_cn' => array(
                'between'  => array(
                    'rule'    => array('between', 0, 128),
                    'message' => 'Between 0 to 128 characters'
                ),
            ),
            'subject'    => array(
                'notEmpty' => array(
                    'rule'    => 'notEmpty',
                    'message' => __('Subject can not empty'),
                ),
                'between'  => array(
                    'rule'    => array('between', 1, 128),
                    'message' => 'Between 1 to 128 characters'
                ),
            ),
            'subject_en' => array(
                'between'  => array(
                    'rule'    => array('between', 0, 128),
                    'message' => 'Between 0 to 128 characters'
                ),
            ),
            'subject_cn' => array(
                'between'  => array(
                    'rule'    => array('between', 0, 128),
                    'message' => 'Between 0 to 128 characters'
                ),
            ),
            'memo' => array(
                'between'  => array(
                    'rule'    => array('between', 0, 255),
                    'message' => 'Between 0 to 255 characters'
                ),
            )

        );
        if ($this->validates()) {
            return true;
        }

        return false;
    }
}

