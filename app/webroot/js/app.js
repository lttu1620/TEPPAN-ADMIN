/*===================================================================================================*/
/* Light box mita-mitai
/*===================================================================================================*/

function lightbox_outner() {
	lightbox_inner();
}

	function lightbox_inner() {
		$('#modal_lightbox').modal('view',{
			speed : 800,
			easing : 'easeInOutElastic',
			animation : 'fade',
			position: 'center',
			overlayClose : true,
			on : 'click',
			close : '.close'
		});
}

function lightbox_nolog() {
	/* js change tab when nologin */
	setTimeout(function() {
		$(this).addClass('active');
		var tabHeader = $('.tab-head-content > div');
		tabHeader.hide().filter(':first').show();
	});
	/* js call modal tab when nologin */
	lightbox_nolog_inner();
}

	function lightbox_nolog_inner() {
		$('#modal_lightbox_nolog').modal('view',{
			speed : 800,
			easing : 'easeInOutElastic',
			animation : 'fade',
			position: 'center',
			overlayClose : true,
			on : 'click',
			close : '.btnclose'
		});
	}

$(function () {

	/*===================================================================================================*/
	/* JS Tabs Header and Tabs common
	/*===================================================================================================*/
	// actSelector(null, null);
	var tabHeader = $('.tab-head-content > div');
	tabHeader.hide();

	$('#tab-head-bar a').each(function(index) {
		var dataTab = $(this).attr('data-content');

		if($(this).hasClass('active')) {
			tabHeader.filter(':nth-child('+dataTab+')').show();
		}
		$(this).click(function(event) {
			/* Act on the event */
			event.preventDefault();
			tabHeader.hide();
			tabHeader.filter(this.hash).show();
			$('#tab-head-bar a').removeClass('active');
			$(this).addClass('active');

			return false;
		});
	});

	$('.tab-head-content ul li a').click(function() {
		$('.tab-head-content ul li a').removeClass('active');
		$(this).addClass('active');
	});

	/*===================================================================================================*/
	/* Variable of selector in body
	/*===================================================================================================*/
	var boxMovieSearch = $('#movieSearch'); /* PM140 */
	var parentMovieSearch = $('.box-movieSearch__parent'); /* PM140 */
	var h_wrapAll = $('#wrap--all').innerHeight();
	var heightBody = $('body').innerHeight();
	var heightWindow = $(window).innerHeight();
	var heightDocument = $(document).innerHeight();
	var h_MenuSlide = $('#scroller').innerHeight();
	var see_list = $('#see_list'); /* Top */

	/*===================================================================================================*/
	/* Variable of detect Mobile (iOS)
	/*===================================================================================================*/
	var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
	};

	/*===================================================================================================*/
	/* JS check zoom for input, textarea, select
	/*===================================================================================================*/
	if(isMobile.iOS()) {
		$("input[type=text], input[type=password], input[type=date], select, textarea").focusin(function() {
			zoomDisable();
		});
		$("input[type=text], input[type=password], input[type=date], select, textarea").focusout(function() {
			zoomEnable();
		});
	}

	/*===================================================================================================*/
	/* Call function ellipsis text for PM130 page
	/*===================================================================================================*/
	// $('.list-name').ellipsis();

	/*===================================================================================================*/
	/* JS fixed selector .wrap-calendar of PM030 page
	/*===================================================================================================*/	
	var $sel_Calendar = $('.wrap-calendar');
	var pos = $sel_Calendar.offset();

	if($sel_Calendar.length >= 1) {

		$(window).scroll(function() {
      var y = $(window).scrollTop();
      if(y >= pos.top) {
        $sel_Calendar.addClass('wrap-calendar-fix');
        $('.page').parent().removeClass('page--slide');
      } else {
        $sel_Calendar.removeClass('wrap-calendar-fix');
        $('.page').parent().addClass('page--slide');
      }
    });

	}

	/*========================================================================================s===========*/
	/* JS User Profile M001 page
	/*===================================================================================================*/ 
  $('.user-dropdown--profile').on('click', function(event) {
    event.preventDefault();
    $('#marks-page').show().css({
			'height': h_wrapAll
		});
    $('.list-select').toggleClass('is-visible');
  });

	$('.user-dropdown .list-select a').click(function() {
		var indexSrc = $(this).find('img').attr('src');
		$(this).parents('.list-select').children('li').show();
		$(this).parent().hide();
		$('.ic-main-pro').attr('src', indexSrc);
	});

	/*===================================================================================================*/
	/* JS Sidebar Navigation Header
	/*===================================================================================================*/
	var hideLeftOptions = {'direction':'left', 'mode':'hide'};
	var hideRightOptions = {'direction':'right', 'mode':'hide'};

  	$('html').bind('click', function(e) {
  		var target = e ? e.target : window.event.srcElement;
  		if((target.id !== 'anchor-menu') && (target.id !== 'ic-anchor')) {
  			$('.menu').removeClass('menu-push');
      	$('.page').removeClass('menu-push-right');
      	$('.page').parent().removeClass('wrap-push-right');
      	$('.page').css({
      		'height' : 'auto'
      	});
  		}
  	});
    if (jQuery.ui) {
        $('html').bind('click', function (e) {
            e.stopPropagation();
            var target = e ? e.target : window.event.srcElement;
            // alert(target.id);
            if ((target.id !== 'event-profile') && (target.id !== 'img-profile') && (target.id !== 'see_list')
                    && (target.id !== 'icon-gear') && (target.id !== 'a_prev') && (target.id !== 'a_next')
                    && (target.id !== 'span_prev') && (target.id !== 'span_next') && (target.id !== 'tipsy_one')
                    && (target.id !== 'tipsy_two') && (target.id !== 'tipsy_three') && (target.id !== 'tipsy_one_span')
                    && (target.id !== 'tipsy_two_span') && (target.id !== 'tipsy_three_span')
                    && (target.id !== 'tipsy_two_number') && (target.id !== 'tipsy_three_number') && (target.id !== 'tipsy_one_number'))
            {
                $('.page').parents('#wrap--all').removeClass('event-pointer');
                $('#marks-page').hide();
                setTimeout(function () {
                    $('.list-select').removeClass('is-visible');
                    $('.tipsy').removeClass('hover');
                    see_list.effect("slide", hideRightOptions, 700);
                }, 500);
            }
        });
    }
                

  /*===================================================================================================*/
	/* JS toggle list M020
	/*===================================================================================================*/
	var heightList = $('.see_list').innerHeight();

	$('#icon-gear, .name_title').click(function(event) {
		event.preventDefault();

		$('#marks-page').show().css({'height': h_wrapAll});
		$('.page').parents('#wrap--all').addClass('event-pointer');
		$('.see_list').toggle("slide", {direction:'right'}, 700);

		if(heightBody <= heightList) {
			tamp = heightList;
			$('.page').css({
				'height' : tamp + 100
			});
		}

	});
	/* end JS toogle list M020
	/*===================================================================================================*/
  	
    $('.nav--menu').on('click', function(event) {
      event.preventDefault();
      $('.menu').toggleClass('menu-push');
      $('.page').toggleClass('menu-push-right');
      $('.page').parent().toggleClass('wrap-push-right');
      $('.page').css({
      	'height' : h_MenuSlide + 'px'
      });
    });

    $('.menu').find('a').on('click', function(event) {
      setTimeout(function() {
        $('.menu').removeClass('menu-push');
      	$('.page').removeClass('menu-push-right');
      	$('.page').parent().removeClass('wrap-push-right');
      }, 300);    
    });

    $('.nav--search').on('click', function(event) {
      event.preventDefault();
      var $self = $(this);

      $self.siblings('.arrow').toggleClass('is-hidden');
      $('#search').toggleClass('is-hidden');
    });

	/*===================================================================================================*/
	/* Touch event for page Top and M020
	/*===================================================================================================*/
	var see_list_ID = document.getElementById('see_list');
	var boxMovieSearch_ID = document.getElementById('movieSearch');

  if($('body').find('#see_list').length >= 1) {
  	touchEvent(see_list_ID, see_list, 'swiperight', 'right');
  }

  if($('body').find('#movieSearch').length >= 1) {
  	touchEvent(boxMovieSearch_ID, boxMovieSearch, 'swipeleft', 'left');
  }

	/*===================================================================================================*/
	/* JS show/hide box Movie search for page PM140
	/*===================================================================================================*/
	boxMovieSearch.hide();
	$('.btn-movieSearch, .btn-movieClose').click(function(event) {
		event.preventDefault();
	    $("#list_content_search_eq_my_best_movie").html("");
	    $("#list_content_search_my_best_movie").html("");
	    $("#show_count_search_my_best_movie").html("");
	    $("#show_count_search_eq_my_best_movie").html("");
	    $("#list_content_search_my_best_movie").next().hide();
		boxMovieSearch.toggle("slide",{direction:'left'}, 700);
	});

	$('.box-movieSearch li').click(function() {
			var self = $(this);
			/* Act on the event */
			var titleMovie = self.find('.ui-link').text();
			$('.chosen-movie').show().html(titleMovie);
			$('.btn-movieSearch').css('float','right');
		});
	/*===================================================================================================*/
	/* end JS show/hide box Movie search for page PM140
	/*===================================================================================================*/

	/*===================================================================================================*/
	/* JS Accordion of PM080_QA
	/*===================================================================================================*/
	var ulContent = $('.accordionQA ul');
  ulContent.hide();
  $('.accordionQA .title-item').click(function(event) {
    event.preventDefault();
    /* Act on the event */
    var selfTitle = $(this);
    selfTitle.parent().find(ulContent).fadeToggle('medium');
  });
  /*===================================================================================================*/
	/* JS Accordion of PM080_QA end
	/*===================================================================================================*/

	/*===================================================================================================*/
	/* check enter number for .text_numberic	
	/*===================================================================================================*/
	$(".text_numberic").keydown(function (e) {
		if (e.shiftKey || e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down 
			e.preventDefault();         // Prevent character input 
		} else {
			var n = e.keyCode;
			if (!((n == 8)              // backspace 
            || (n == 46)                // delete 
            || (n >= 35 && n <= 40)     // arrow keys/home/end 
            || (n >= 48 && n <= 57)     // numbers on keyboard 
            || (n >= 96 && n <= 105))   // number on keypad 
            ) {
				e.preventDefault();     // Prevent character input 
			}
		}
	});

	/*===================================================================================================*/
	/* JS Deleted dayPicker page PM130 
	/*===================================================================================================*/
	removeDatePicker('#del_fromDay', '#fromDay');
	removeDatePicker('#del_toDay', '#toDay');

  if ($('#accordion-more').length > 0) {
    $('#accordion-more').click(function() {
      var $self = $(this),
          $img = $self.find('img');

      if ($self.hasClass('active')) {
        $self.removeClass('active');
        $img.attr('src', $img.attr('src').replace(/btn_summary_more/, 'btn_summary_close'));
        $('.review').slideDown(300);
      } else {
        $self.addClass('active');
        $img.attr('src', $img.attr('src').replace(/btn_summary_close/, 'btn_summary_more'));
        $('.review').slideUp(300);
      }
    });
  }

	/*===================================================================================================*/
	/* JS slider and expander of page PM120
	/*===================================================================================================*/
 	sliderChange('.slider__rank');
 	var heightRankHeader = $('.rank__header').height();
 	expanderSlide('.rank__part', '.rank__part__body__detail', '.bxSlider__rank', '.rank__part__event', heightRankHeader, 5);

 	
  /*===================================================================================================*/
	/* JS Toggle expander of page PM040
	/*===================================================================================================*/
  expanderEvent('.cinema', '.block_body_detail', 'a.block_tg', 3);

	/*===================================================================================================*/
	/* JS for Search Girl images 
	/*===================================================================================================*/
	$('.category-movie a.e-search').click(function() {
		/* Act of event */
		var self = $(this);
		self.parents('.category-movie').find('.thumb-search').attr('src','../../common/img/girl_2.png');

		$('#tab-head-bar ul>li>a').removeClass('active');
		$('#tab-head-bar ul>li:nth-child(3)>a').addClass('active');
		var tabHeader = $('.tab-head-content > div');
		tabHeader.hide().filter(':nth-child(3)').show();

		setTimeout(function() {
			//window.location.href = newReturnLocationpath() + 'public/cinema/cinema_result.aspx';
			$('html, body').animate({ scrollTop : 0	}, 1000).find('.i-search').focus();	//.attr('placeholder', 'Can you search this Girl at here !!!')
		}, 1000);
		clearTimeout();
		// actSelector("#id__find", "");
	});

	/*===================================================================================================*/
	/* Checkbox Movie 
	/*===================================================================================================*/
	$('.cate-movie .movie-item').click(function() {
		/* Act of event */
		var myCheck = $(this);
		myCheck.parent('li').toggleClass('checked');
		myCheck.parent('li').find('.check-box').toggleClass('ui-icon16-501');
	});

	/*===================================================================================================*/
	/* Back to top
	/*===================================================================================================*/
	$('.back-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	});

	var $linkToTop = $('.linkToTop');
	if ($linkToTop.length >= 1) {

		var h_wrapAll = $('#wrap--all').innerHeight();
		var heightWindow = $(window).innerHeight() + 100;
		if ($linkToTop.length >= 1) {
			if (h_wrapAll < heightWindow) {
				$linkToTop.css({ 'display': 'none' });
			} else {
				returnTop();
			}
		}
	}

	/*===================================================================================================*/
	/* JS Tabs common
	/*===================================================================================================*/
	var tabSelected = $('.tab-content > div');
	tabSelected.hide().filter(':first').show().addClass('tabSelected');

	$('#tab-bar a').click(function() {
		
		tabSelected.hide().removeClass('tabSelected');
		tabSelected.filter(this.hash).show().addClass('tabSelected');
		$('#tab-bar a').removeClass('active');
		$(this).addClass('active');

		/* JS Change title when active tag page PM130 
		=================================================*/
	  changeTitle('li:nth-child(1) a', '.pri-title', '作品');
	  changeTitle('li:nth-child(2) a', '.pri-title', 'ユーザー');
	  changeTitle('li:nth-child(3) a', '.pri-title', 'レビュー（作品名を表示）');
	  changeTitle('li:nth-child(4) a', '.pri-title', 'ラック名');
	  /* end JS Change title when active tag page PM130
		=================================================*/
		return false;
	});

	/*===================================================================================================*/
	/* JS show/hide table content of page PM130
	/*===================================================================================================*/
	$('.event-order').click(function() {
		var selfArrow = $(this);
		if(selfArrow.parents('.wrap-inner__top').find('.arrow-order').hasClass('ui-icon24-610')) {
	  	selfArrow.parents('.wrap-inner__top').find('.arrow-order').removeClass('ui-icon24-610');
	  	selfArrow.parents('.wrap-inner__top').find('.arrow-order').addClass('ui-icon24-609');
	  } else {
	  	selfArrow.parents('.wrap-inner__top').find('.arrow-order').addClass('ui-icon24-610');
	  	selfArrow.parents('.wrap-inner__top').find('.arrow-order').removeClass('ui-icon24-609');
	  }
		selfArrow.parents('.wrap-ranking__inner').find('.wrap-inner__detail').slideToggle('slow');
	});
	/*===================================================================================================*/
	/* End JS show/hide table content of page PM130
	/*===================================================================================================*/

	/*===================================================================================================*/
	/* Slide banner top
	/*===================================================================================================*/
	if ($('.slide-banner__top .slide-ads').length > 0) {
		var sliderAds = $('.slide-banner__top .slide-ads').bxSlider({
			auto: true,
      controls: true,
      pager: false 
		});
	}

	$('.slide-banner__top .bx-next, .slide-banner__top .bx-prev')
    	.on('mouseenter', function() { sliderAds.stopAuto(); })
    	.on('mouseleave', function() { sliderAds.startAuto(); });

}); /* end document jquery */

/*===================================================================================================*/
/* Function get attribute YYYY/MM/DD for input DateTime                 
/*===================================================================================================*/
function getPlaceHolder(item) {
	var year = null,
	month = null,
	date = null;
	var now = new Date();
	year = now.getFullYear();
	month = now.getMonth() + 1;
	date = now.getDate();

	$(item).attr('placeholder', year + '年' + month + '月' + date + '日（木）');
}

/*===================================================================================================*/
/* menu tree                             
/*===================================================================================================*/
$(document).ready(function () {
	$('label.tree-toggler').parent().children('ul.tree').hide();
	$('label.tree-toggler').click(function () {
		$(this).parent().children('ul.tree').toggle(300);
	});
	$('ul.tree > li > a').click(function () {
		$('label.tree-toggler').parent().children('ul.tree').hide();
	});
});

/*===================================================================================================*/
/* Function Remove DatePicker PM140       
/*===================================================================================================*/
function removeDatePicker(item, target) {
	$(item).click(function() {
		
		/* Act for event */
		$(target).val('');
	});
}

/*===================================================================================================*/
/* Function change title tab PM130         
/*===================================================================================================*/
function changeTitle(tab, title, value) {
	if($(tab).hasClass('active')) {
		$(title).text(value);
	}
}

/*===================================================================================================*/
/* Mita/Mitai click                       
/*===================================================================================================*/
function btnmitaclick($that) {
	$that.parent('.wrap-mita-mitai').find('.part-saw').fadeToggle('slow');
}
/*===================================================================================================*/
/* Datepicker                        
/*===================================================================================================*/
function getDatePicker(item) {
		if(item.length > 0) {
			$(item).datepicker({
			showOn: 'both',
			dayNames: ['日', '月', '火', '水', '木', '金', '土'],
			dateFormat: 'yy/mm/dd',
			buttonImage: '../../common/img/icons/calendar.png',
			buttonImageOnly: true
			});
		}
	}

var base_ajax_time_out = 120000;
var base_url_system_error = "../../contents/error/index.aspx";
var base_ajax_url_back_is_not_logon = "../../public/entry";
var base_ajax_time_out_message = "システムタイムアウトを発生しました。";

/*===================================================================================================*/
/* format numberic                         
/*===================================================================================================*/
	function addCommas(str) {
	var amount = new String(str);
	amount = amount.split("").reverse();
		var output = "";
		for (var i = 0; i <= amount.length - 1; i++) {
			output = amount[i] + output;
			if ((i + 1) % 3 == 0 && (amount.length - 1) !== i) output = ',' + output;
		}
	return output;
	}

/*===================================================================================================*/
/* string extend function                  
/*===================================================================================================*/
	String.prototype.replaceAll = function (find, replace) {
		var str = this;
		return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
	};

/*===================================================================================================*/
/* Touch Event function                  
/*===================================================================================================*/
function touchEvent(el, sel, optSwipe, posUi) {
	Hammer(el).on(optSwipe, function() {
	    sel.toggle("slide", { direction: posUi }, 700);
	    $("#list_content_search_eq_my_best_movie").html("");
	    $("#list_content_search_my_best_movie").html("");
	    $("#show_count_search_my_best_movie").html("");
	    $("#show_count_search_eq_my_best_movie").html("");
	    $("#list_content_search_my_best_movie").next().hide();
	});
}

function zoomDisable(){
  $('head meta[name=viewport]').remove();
  $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />');
}
function zoomEnable(){
  $('head meta[name=viewport]').remove();
  $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />');
}

/*===================================================================================================*/
/* Function fade Button meta/metai                  
/*===================================================================================================*/
function bind_event_button_mita_mitai() {
	$('.btn_group>a').click(function (event) {
		/* Act on the event */
		event.preventDefault();
		$(this).parent('div').find('ul').toggle();
	});

	$('.btn_group li a').click(function (event) {
		$(this).parents('.btn_group').find('ul').fadeOut();
	});
}

//Tuancd - html decode and encode using jquery
function htmlEncode(value) {
	//create a in-memory div, set it's inner text(which jQuery automatically encodes)
	//then grab the encoded contents back out.  The div never exists on the page.
	value = value.replaceAll("'", "\"");
	return $('<div/>').text(value).html();
}

function htmlDecode(value) {
	return $('<div/>').html(value).text();
}

/*===================================================================================================*/
/* JS slider and expander of page PM120 02-01-2014
/*===================================================================================================*/
function sliderChange(element) {
	if($(element).length >=1) {
		var sliderRanking = $(element).bxSlider({
			auto: false,
			autoControls: false,
			adaptiveHeight: true
		});
	}
}

function expanderSlide(selector, content, slideRank, button, heightRankHeader, rows) {
	var $rankPart = $(selector);
	$.each($rankPart, function(index, selector) {
		var $selfRankPart = $(this);
		var $content = $selfRankPart.find(content);
		var $slideRank = $selfRankPart.parents(slideRank);
		var $btnReadMore = $selfRankPart.find(button);

		var heightContent = $content.height();
		var lineHeight = $content.css('line-height');
		lineHeight = parseFloat(lineHeight);
		var rowsIndex = Math.round(heightContent/lineHeight);

 		var settingHeight = 0;
 		var tamp = 0;

 		if(rowsIndex > rows) {
 			settingHeight = rows*lineHeight;
 			$content.css({
 				'height' : settingHeight,
 				'overflow' : 'hidden'
 			});
 			var heightRankPart = $selfRankPart.height();
 			tamp = heightRankPart + heightRankHeader + 10;
 			$slideRank.find('.bx-viewport').height(tamp);
 			// console.log(tamp);

 			$btnReadMore.click(function(event) {
 				event.preventDefault();
 				var $selfBtnReadMore = $(this);
 				if($selfBtnReadMore.hasClass('active')) {
 					$selfBtnReadMore.removeClass('active');
 					$selfBtnReadMore.html('▼ もっと見る');
 					$content.css({
 						'height' : settingHeight,
 						'overflow' : 'hidden'
 					});
 					var heightRankPart = $selfRankPart.height();
 					tamp = heightRankPart + heightRankHeader + 10;
 					$slideRank.find('.bx-viewport').height(tamp);
 				} else {
 					$selfBtnReadMore.addClass('active');
 					$selfBtnReadMore.html('▲ 閉じる');
 					$content.css({
 						'height' : 'auto',
 						'overflow' : 'hidden'
 					});
 					var heightRankPart = $selfRankPart.height();
 					tamp = heightRankPart + heightRankHeader + 10;
 					$slideRank.find('.bx-viewport').height(tamp);
 				}
 			});
 		} else {
 			$btnReadMore.hide();
 		}
	});
}

/*=========================================*/
/* Expander Common New 01-02-2014
/*=========================================*/
function expanderEvent(selector, content, button, rows) {
	var $selector = $(selector);
	$.each($selector, function (index, selector) {
		var $selfSelector = $(this);
		// check element is set expander
		if ($selfSelector.data("isExpander"))
			return;

		var $content = $selfSelector.find(content);
		var $btnEvent = $selfSelector.find(button);

		var heightContent = $content.height();
		var lineHeight = $content.css('line-height');
		lineHeight = parseFloat(lineHeight);
		var rowsIndex = Math.round(heightContent / lineHeight);

		var settingHeight = 0;

		if (rowsIndex > rows) {
			settingHeight = rows * lineHeight;

			$content.css({
				'height': settingHeight,
				'overflow': 'hidden'
			});

			$btnEvent.click(function (event) {
				event.preventDefault();
				var $selfEvent = $(this);

				if ($selfEvent.hasClass('active')) {
					$selfEvent.removeClass('active');
					$content.css({ 'height': settingHeight });
				} else {
					$selfEvent.addClass('active');
					$content.css({ 'height': 'auto' });
				}
			});

		} else {
			$btnEvent.hide();
		}
		$selfSelector.data("isExpander", true);
	});
}

/*===================================================================================================*/
/* Plugin ellipsis text 
/*===================================================================================================*/
(function($) {
	$.fn.ellipsis = function()
	{
		return this.each(function()
		{
			var el = $(this);

			if(el.css("overflow") == "hidden")
			{
				var text = el.html();
				var multiline = el.hasClass('multiline');
				var t = $(this.cloneNode(true))
					.hide()
					.css('position', 'absolute')
					.css('overflow', 'visible')
					.width(multiline ? el.width() : 'auto')
					.height(multiline ? 'auto' : el.height())
					;

				el.after(t);

				function height() { return t.height() > el.height(); };
				function width() { return t.width() > el.width(); };

				var func = multiline ? height : width;

				while (text.length > 0 && func())
				{
					text = text.substr(0, text.length - 1);
					t.html(text + "...");
				}

				el.html(t.html());
				t.remove();
			}
		});
	};
})(jQuery);

/*===================================================================================================*/
/* Remove href nologin top
/*===================================================================================================*/

$(function () {
	$('.head_block .menu-disabled a').removeAttr('href');
	$('.no_log li:first a').removeAttr('href');
	$('.logged li:first a').removeAttr('onclick');
});

/*===================================================================================================*/
/* Function active selector when Tab click
/*===================================================================================================*/
function actSelector(tabParent, tabChild) {
	var tabHeader = $('.tab-head-content > div');
	tabHeader.hide();

	/* Active selector */
	$(tabParent).parents('#tab-head-bar').find('a').removeClass('active');
	$(tabParent).addClass('active');
	var dataTab = $(tabParent).attr('data-content');
	tabHeader.filter(':nth-child(' + dataTab + ')').show();

	/* Active default */
	$('#tab-head-bar a').each(function (index) {
		var dataTab = $(this).attr('data-content');

		if ($(this).hasClass('active')) {
			tabHeader.filter(':nth-child(' + dataTab + ')').show();
		}
		$(this).click(function (event) {
			/* Act on the event */
			event.preventDefault();
			tabHeader.hide();
			tabHeader.filter(this.hash).show();
			$('#tab-head-bar a').removeClass('active');
			$(this).addClass('active');

			return false;
		});
	});

	if (tabChild != "") {
		$(tabChild).parents('.tab-head-content').find('a').removeClass('active');
		$(tabChild).addClass('active');
	}

}

/*===================================================================================================*/
/* Function change Icon profile
/*===================================================================================================*/
function changeIconPro(selector) {
	var $changeImg = $(selector).find('img');
  var indexSrc = $changeImg.attr('src');

  $(selector).parents('.list-select').children('li').show();
  $(selector).parent().hide();
	$('.ic-main-pro').attr('src', indexSrc);
}

function returnTop() {
	$(window).scroll(function () {
		var $linkToTop = $('.linkToTop');
		var posScroll = $(window).scrollTop();
		var w_Height = $(window).innerHeight();
		var d_Height = $(document).innerHeight();
		var discut = d_Height - w_Height - 100;
		$linkToTop.css({ 'display': 'block' });

		switch(posScroll) {
			case 0:
				$linkToTop.hide();
				break;
			case 100:
				$linkToTop.addClass('fixed-linkToTop');
				if(posScroll >= discut) {
					$linkToTop.removeClass('fixed-linkToTop');
				}
				break;
			default:
				$linkToTop.removeClass('fixed-linkToTop');
				break;
		}
	});
}
