<?php

class cacheObj {

    public $key = null;
    public $seconds = 86400; // 24*60*60

    public function __construct($key = null, $seconds = null) {
        $this->key = $key;
        $this->seconds = $seconds;
    }

}

Configure::write('universities_all_key_value', new cacheObj('universities_all_key_value', 60 * 60));
Configure::write('universities_all', new cacheObj('universities_all', 60 * 60));
Configure::write('campuses_all', new cacheObj('campuses_all', 60 * 60));
Configure::write('departments_all', new cacheObj('departments_all', 60 * 60));
Configure::write('mail_situations_all_key_value', new cacheObj('mail_situations_all_key_value', 60 * 60));
Configure::write('mail_types_all_key_value', new cacheObj('mail_types_all_key_value', 60 * 60));
Configure::write('users_all_key_value', new cacheObj('users_all_key_value', 60 * 60));
Configure::write('templates_all_key_value', new cacheObj('templates_all_key_value', 60 * 60));
Configure::write('report_general', new cacheObj('report_general', 60 * 60));