<?php

class AppEmail extends CakeEmail {

    public static function test($to = 'developer.php.vn@gmail.com') {
        $subject = __('Ｔｅｓｔ');
        $message = 'ＳＭＴＰ';
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->to($to);
        $Email->subject($subject);
        if (!$Email->send($message)) {
            return false;
        }
        return true;
    }
    
    public static function sendMailRegister($to) {
        $subject = __('Confirm email');
        $message = Router::url("/register/active?token={$to['token']}");
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->to($to['email']);
        $Email->subject($subject);
        if (!$Email->send($message)) {
            return false;
        }
        return true;
    }

    public static function sendMailRequestCompany($to) {
        $subject = __('Request company');
        $message = Router::url("/register/approve?token={$to['token']}");
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->to($to['email']);
        $Email->subject($subject);
        if (!$Email->send($message)) {
            return false;
        }
        return true;
    }

    public static function sendMailForgetPassword($to, $token) {
        $subject = __('Forget Password');
        $message = "Get new password form OCEANIZE \n We are send you a link to renew password. It expired after 24h. ";
        $link = Router::url("/pages/newpassword?token={$to['token']}");
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->to($to);
        $Email->subject($subject);
        if (!$Email->send($message . " Please click $link  to get new password.")) {
            return false;
        }
        return true;
    }

     public static function sendMailApprove($to) {
        $subject = __('Your request has been approved');
        $message = Router::url('/login');
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->to($to['email']);
        $Email->subject($subject);
        if (!$Email->send($message)) {
            return false;
        }
        return true;
    }
}
