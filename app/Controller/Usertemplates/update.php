<?php

$modelName = $this->UserTemplate->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add user template');
if (!empty($id)) {
    // call API get template detail
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_user_templates_detail'), $param);
    $this->Common->handleException(Api::getError());
    $pageTitle = __('Edit user template');
}

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));
$this->setPageTitle($pageTitle);
// create update form - Information Form
$this->UpdateForm
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'        => 'id',
        'type'      => 'hidden',
        'label'     => __('Id'),
    ))
    ->addElement(array(
        'id'        => 'title',
        'label'     => __('Title'),
        'required'  => true
    ))
    ->addElement(array(
        'id'        => 'content',
        'type'      => 'textarea',
        'label'     => __('Content'),
        'rows'      => 12
    ))
    ->addElement(array(
        'type'      => 'submit',
        'value'     => __('Save'),
        'class'     => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type'      => 'submit',
        'value'     => __('Cancel'),
        'class'     => 'btn btn-primary pull-left btn-tp',
        'user_id'   => $data[$modelName]['user_id']
    ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_user_templates_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            return $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        } else {
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
} else {
    $this->data = $data;
}