<?php
$this->layout = 'template';

$this->AppHtml->script('plugins/hightchart/highcharts.js');

// create breadcrumb
$pageTitle = __('DAU');
$this->Breadcrumb->setTitle($pageTitle)->add(array(
    'name' => $pageTitle,
));
$param = $this->getParams(array(
    'type' => 'column',
    'mode' => 'day',
));
if (!isset($param['date_from'])) {
    $param['date_from'] = date('Y-m-d', strtotime('last month'));
    $this->setParam('date_from', $param['date_from']);
}
if (!isset($param['date_to'])) {
    $param['date_to'] = date('Y-m-d');
    $this->setParam('date_to', $param['date_to']);
}
// create search form 
$this->SearchForm
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id' => 'date_from',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date from'),
        'value' => $param['date_from']
    ))
    ->addElement(array(
        'id' => 'date_to',
        'type' => 'text',
        'calendar' => true,
        'label' => __('Date to'),
        'value' => $param['date_to']
    ))
    ->addElement(array(
        'id' => 'type',
        'label' => __('Chart type'),
        'options' => Configure::read('Config.searchChartType'),
    ))    
    ->addElement(array(
        'type' => 'submit',
        'id' => 'btnSearch',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));

$dau = Api::call(Configure::read('API.url_reports_dau'), $param);
if (Api::getError()) {
    return $this->Common->handleException(Api::getError()); 
}
$this->set('dau', $this->Chart->render(array(
    'id' => 'dau',
    'type' => $param['type'],
    'title' => __('DAU'),
    'data' => $this->Common->arrayDateForChart($dau, 'date'),
    'x' => 'date',
    'y' => array(
        'count_user_templates' => '利用テンプレ',
        'count_template_favorites' => 'お気に入りテンプレ',
        'count_template_open_logs' => '見るテンプレ',
        'count_template_send_logs' => '送信テンプレ',
    )
)));