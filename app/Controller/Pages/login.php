<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};
// Check remember userName and password
$this->Cookie->httpOnly = true;
if (!$this->Auth->loggedIn()) {
    $isAdmin = ($this->request->url == 'login') ? true : false;
    $this->set('isAdmin', $isAdmin);
}

$adminLoginForm = $this->UpdateForm->reset();
$adminLoginForm->setModelName($modelName)
    ->setAttribute('type', 'post')
    ->addElement(array(
        'id' => 'login',
        'label' => false,
        'value' => empty($adm_cookie['login']) ? '' : $adm_cookie['login'],
        'placeholder' => __('Login ID')
    ))
    ->addElement(array(
        'id' => 'password',
        'type' => 'password',
        'label' => false,
        'value' => empty($adm_cookie['admin_password']) ? '' : $adm_cookie['admin_password'],
        'placeholder' => __('Password')
    ))
    ->addElement(array(
        'id' => 'remembera',
        'type' => 'checkbox',
        'value' => '1',
        'checked' => empty($adm_cookie['remembera']) ? false : $adm_cookie['remembera'] == 1,
        'after' => "<label for='remembera' > <span class='nomarlText'> &nbsp;" . __('Remember me') . "</span></lable>",
        'label' => false,
        'class' => ''
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Login'),
        'class' => 'btn bg-capture-red btn-block',
        'style' => 'width:320px;',
    ));
$this->set('adminLoginForm', $adminLoginForm->get());

if ($this->request->is('post')) {
    $isAdminUrl = "login";
    if ($model->validateLogin($this->getData($modelName))) {
        $param['password'] = $model->data[$modelName]['password'];
        if (!empty($model->data[$modelName]['login'])) {
            $param['login_id'] = $model->data[$modelName]['login'];
            if ($this->startAdminLogin($param)) {
                // did they select the remember me checkbox?
                if ($model->data[$modelName]['remembera'] == 1) {
                    $model->data[$modelName]['admin_password'] = $model->data[$modelName]['password'];
                    unset($model->data[$modelName]['password']);
                }
                return $this->redirect($this->Auth->redirect());
            }
        }
    }
    AppLog::info("Login fail", __METHOD__, $this->data);
    if ($isAdmin) {
        $this->Common->setFlashErrorMessage(__('Invalid Login ID or password. Please try again'));
    } else {
        $this->Common->setFlashErrorMessage(__('Invalid Email or password. Please try again'));
    }
}