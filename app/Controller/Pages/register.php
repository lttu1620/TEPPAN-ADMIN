<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};

$this->UpdateForm->setModelName($modelName)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email'),
            'value' => isset($this->data[$modelName]['email']) ? $this->data[$modelName]['email'] : '',
        ))
        ->addElement(array(
            'id' => 'password',
            'type' => 'password',
            'label' => __('Password'),
            'value' => '',
        ))
        ->addElement(array(
            'id' => 'password_confirm',
            'type' => 'password',
            'label' => __('Confirm Password'),
            'value' => '',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Register'),
            'class' => 'btn bg-capture-red btn-block',
            'style' => 'width:320px;',
        ));

if ($this->request->is('post')) {    
    if ($model->validateRegister($this->getData($modelName))) {         
        $param['email'] = $model->data[$modelName]['email'];
        $param['password'] = $model->data[$modelName]['password'];
        $param['regist_type'] = 'register_recruiter';         
        $resendLink = "resendRegister('" . Router::url('/ajax/resendregisteremail') . "','{$param['email']}');return false;";        
        Api::call(Configure::read('API.url_users_registeremail'), $param);
        if (Api::getError()) {
            if (isset(Api::getError()['email'][1021])) {
                $error = array(
                    'email' => array(                        
                        1021 => __('Email address is already registered and waiting activation') . "<br/><a href=\"#\" onclick=\"{$resendLink}\" class=\"resendUrl\">" . __("Resend email for me") . "</a>",
                    )
                );
            } else {
                $error = array(
                    'email' => array(
                        1011 => __('Email address is already registered'),                    
                    )
                );
            }
            return $this->Common->setFlashErrorMessage(Api::getError(), $error);
        }         
        return $this->Common->setFlashSuccessMessage(                
            __('Please check email to activate account')
            . ".<br/><a href=\"#\" onclick=\"{$resendLink}\" class=\"resendUrl\">" . __("Resend email for me") . "</a>"
        );
    }
    return $this->Common->setFlashErrorMessage($model->validationErrors);  
}