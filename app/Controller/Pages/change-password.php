<?php

$modelName = $this->Page->name;
$model = $this->{$modelName};
$adminLoginForm = $this->UpdateForm->setModelName($modelName)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'login',
            'type' => 'input',
            'label' => __('Login ID'),
            'value' => '',
        ))
        ->addElement(array(
            'id' => 'password',
            'type' => 'password',
            'label' => __('Password'),
            'value' => '',
        ))
        ->addElement(array(
    'type' => 'submit',
    'value' => __('Login'),
    'class' => 'btn bg-olive btn-block',
        ));
$this->set('adminLoginForm', $adminLoginForm->get());

$this->UpdateForm->reset();

$userLoginForm = $this->UpdateForm->setModelName($modelName)
        ->setAttribute('type', 'post')
        ->addElement(array(
            'id' => 'email',
            'type' => 'input',
            'label' => __('Email'),
            'value' => '',
        ))
        ->addElement(array(
            'id' => 'password',
            'type' => 'password',
            'label' => __('Password'),
            'value' => '',
        ))
        ->addElement(array(
    'type' => 'submit',
    'value' => __('Login'),
    'class' => 'btn bg-olive btn-block',
        ));
$this->set('userLoginForm', $userLoginForm->get());

if ($this->request->is('post')) {
    $isAdmin = true;
    if ($model->validateLogin($this->getData($modelName))) {
        $param['password'] = $model->data[$modelName]['password'];
        if (!empty($model->data[$modelName]['login'])) {
            $param['login_id'] = $model->data[$modelName]['login'];
            if ($this->startLogin($param)) {
                return $this->redirect($this->Auth->redirect());
            }
        } elseif (!empty($model->data[$modelName]['email'])) {
            $isAdmin = false;
            $param['full'] = true;
            $param['email'] = $model->data[$modelName]['email'];
            if ($this->startLogin($param)) {
                return $this->redirect($this->Auth->redirect());
            }
        }
    }
    AppLog::info("Login fail", __METHOD__, $this->data);
    if ($isAdmin) {
        $this->Common->setFlashErrorMessage(__('Invalid Login ID or password. Please try again'));
    } else {
        $this->Common->setFlashErrorMessage(__('Invalid Email or password. Please try again'));
    }
}