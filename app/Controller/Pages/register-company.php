<?php

$this->AppHtml->script('autocomplete.js');
$this->AppHtml->script('register_company.js');

$modelName = $this->Page->name;
$model = $this->{$modelName};

if (empty($this->AppUI->company_id)) {
    $companyList = Api::call(Configure::read('API.url_companies_all'), array('disable' => 0));
    $nameList = $this->Common->arrayKeyValue($companyList, 'name', 'name');
    $kanaList = $this->Common->arrayKeyValue($companyList, 'kana', 'kana');
    $this->UpdateForm->setModelName($modelName)
            ->setAttribute('type', 'post')
            ->addElement(array(
                'id' => 'name',
                'label' => __('Name'),
                'options' => $nameList,
                'autocomplete' => true,
                'callback' => 'change_company_name',
                'value' => isset($this->data[$modelName]['name']) ? $this->data[$modelName]['name'] : '',
                'empty' => '',
                'name-list' => json_encode($nameList),
            ))
            ->addElement(array(
                'id' => 'kana',
                'label' => __('Kana'),
                'options' => $kanaList,
                'autocomplete' => true,
                'callback' => 'change_company_kana',
                'value' => isset($this->data[$modelName]['kana']) ? $this->data[$modelName]['kana'] : '',
                'empty' => '',
                'kana-list' => json_encode($kanaList),
            ))
            ->addElement(array(
                'id' => 'btn-register-company',
                'type' => 'submit',
                'value' => __('Register company'),
                'class' => 'btn bg-capture-red btn-block',
                'style' => 'width:320px;',
                'onclick' => "return submit_register_compamy();"
    ));

    if ($this->request->is('post')) {
        if ($model->validateRegisterCompany($this->getData($modelName))) { 
            $param['user_id'] = $this->AppUI->id;
            $param['name'] = $model->data[$modelName]['name'];
            $param['kana'] = $model->data[$modelName]['kana'];
            $result = Api::call(Configure::read('API.url_users_registercompany'), $param);            
            if (empty($result)) {
                return $this->Common->setFlashSuccessMessage(__('System error, please try again'));
            }
            $this->AppUI->company_id = $result['company_id'];
            $this->AppUI->company_name = $result['company_name'];
            $this->AppUI->company_kana = $result['company_kana'];
            if (empty($result['is_admin'])) {                
                $this->AppUI->recruiter_admim = 0;
                $this->AppUI->is_approved = 0;
                $this->AppUI->redirect_register_profile = 0;
                $this->AppUI->redirect_register_company = 1;
                $this->Auth->login($this->AppUI);
                return $this->redirect('/register/company');                
            } else {               
                $this->AppUI->recruiter_admim = 1;
                $this->AppUI->is_approved = 1;
                if (!empty($result['new_company'])) {
                    $this->AppUI->redirect_register_profile = 0;
                    $this->AppUI->redirect_register_company = 1;
                    $this->Auth->login($this->AppUI);
                    return $this->redirect('/register/company');
                } else {                    
                    $this->AppUI->redirect_register_profile = 0;
                    $this->AppUI->redirect_register_company = 0;
                    $this->Auth->login($this->AppUI);           
                    return $this->redirect('/');
                }                
            }
        }
    }
} else {    
    if ($this->AppUI->recruiter_admin == 0 && $this->AppUI->is_approved == 0) {
        $detail = Api::call(Configure::read('API.url_recruiters_detail'), array('user_id' => $this->AppUI->id));
        if (Api::getError()) {
            AppLog::info("API.url_recruiters_detail failed", __METHOD__, $this->AppUI->id);
            return $this->Common->setFlashErrorMessage(Api::getError());
        }  
        if (!empty($detail['is_approved'])) {
            $this->Cookie->write('last_login_url', 'login');
            return $this->redirect('/logout');
        }
        $resendLink = "resendRegister('" . Router::url('/ajax/resendregistercompany') . "','{$this->AppUI->email}');return false;";
        $this->set('resendLink', $resendLink);
        return true;        
    }
    $data = array();
    if (!empty($this->AppUI->company_thumbnail_img) && !empty($this->AppUI->company_has_thumbnail_img)) {
        $data[$modelName]['thumbnail_img'] = $this->AppUI->company_thumbnail_img;
    }
    if (!empty($this->AppUI->company_address)) {
        $data[$modelName]['address'] = $this->AppUI->company_address;
    }
    if (!empty($this->AppUI->company_description_short)) {
        $data[$modelName]['description_short'] = $this->AppUI->company_description_short;
    }
    $categoryList = MasterData::categories_all(array('id', 'name'));
    $this->UpdateForm->setModelName($modelName)
            ->setData($data)
            ->setAttribute('type', 'post')
            ->addElement(array(
                'id' => 'id',
                'type' => 'hidden',
                'label' => __('Id'),
                'value' => $this->AppUI->company_id
            ))
            ->addElement(array(
                'id' => 'thumbnail_img',
                'type' => 'file',
                'image' => true,
                'label' => __('Thumbnail img'),
                'required' => true,
                'crop' => array('company_id' => $this->AppUI->company_id, 'field' => 'thumbnail_img')
            ))
            ->addElement(array(
                'id' => 'address',
                'label' => __('Address'),
                'required' => true
            ))
            ->addElement(array(
                'id' => 'description_short',
                'type' => 'textarea',
                'rows' => 5,
                'label' => __('Short description'),
                'required' => true
            )) 
            ->addElement(array(
                'id' => 'category_id',
                'label' => __('Category Name'),
                'options' => $categoryList,
                'empty' => Configure::read('Config.StrChooseOne'),
                'required' => true
            ))
            ->addElement(array(
                'type' => 'submit',
                'value' => __('Update company'),
                'class' => 'btn bg-capture-red btn-block',
                'style' => 'width:320px;',
    ));

    if ($this->request->is('post')) {
        if ($model->validateUpdateCompany($this->getData($modelName))) {
            //upload image
            if (!empty($_FILES['data']['name'][$modelName]['thumbnail_img'])) {
                $model->data[$modelName]['thumbnail_img'] = $this->Image->uploadImage(
                        "{$modelName}.thumbnail_img", 'companies');
            } elseif (isset($model->data[$modelName]['thumbnail_img'])) {
                unset($model->data[$modelName]['thumbnail_img']);
            }
            $model->data[$modelName]['ower_user_id'] = $this->AppUI->id;            
            Api::call(Configure::read('API.url_companies_addupdate'), $model->data[$modelName]);
            if (Api::getError()) {
                AppLog::info("API.url_companies_addupdate failed", __METHOD__, $model->data[$modelName]);
                return $this->Common->setFlashErrorMessage(Api::getError());
            } else {
                if (isset($model->data[$modelName]['thumbnail_img'])) {
                    $this->AppUI->company_thumbnail_img = $model->data[$modelName]['thumbnail_img'];
                }
                $this->AppUI->company_address = $model->data[$modelName]['address'];
                $this->AppUI->company_description_short = $model->data[$modelName]['description_short'];
                $this->AppUI->redirect_register_profile = 0;
                $this->AppUI->redirect_register_company = 0;
                $this->Auth->login($this->AppUI);           
                return $this->redirect('/');
            }
        } else {
            $this->Common->setFlashErrorMessage($model->validationErrors);
        }
    }
}