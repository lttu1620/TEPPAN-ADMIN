<?php
//include_once ROOT . "/app/Config/auth.php";
$pageTitle = __('TEPPAN 就活メールテンプレート');
$this->setPageTitle($pageTitle);
if ($this->RequestHandler->isMobile()) {
    $this->layout = "page-mobile";
    $this->view = "lp-mobile";
} else {
    $this->layout = "page";
}