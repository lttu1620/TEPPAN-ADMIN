<?php

$modelName = $this->Templateopenlog->name;

// create breadcrumb
$pageTitle = __('Template open log list');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create search form
$this->SearchForm->setModelName($modelName)
        ->setAttribute('type', 'get')
        ->addElement(array(
            'id' => 'name',
            'label' => __('Username')
        ))
        ->addElement(array(
            'id' => 'email',
            'label' => __('Email')
        ))
        ->addElement(array(
            'id' => 'date_from',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date from'),
        ))
        ->addElement(array(
            'id' => 'date_to',
            'type' => 'text',
            'calendar' => true,
            'label' => __('Date to'),
        ))
        ->addElement(array(
            'id' => 'sort',
            'label' => __('Sort'),
            'options' => array(
                'username-asc' => __('Name Asc'),
                'username-desc' => __('Name Desc'),
                'email-asc' => __('Email Asc'),
                'email-desc' => __('Email Desc'),
                'template_title-asc' => __('Title Asc'),
                'template_title-desc' => __('Title Desc'),
                'created-asc' => __('Created Asc'),
                'created-desc' => __('Created Desc'),
            ),
            'empty' => Configure::read('Config.StrChooseOne')
        ))
        ->addElement(array(
            'id' => 'limit',
            'label' => __('Limit'),
            'options' => Configure::read('Config.searchPageSize'),
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Search'),
            'class' => 'btn btn-primary pull-right',
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_templateopenlogs_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
            'id' => 'username',
            'title' => __('Username'),
            'empty' => '',
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'email',
            'title' => __('Email'),
            'empty' => '',
            'width' => '200'
        ))
        ->addColumn(array(
            'id' => 'template_title',
            'title' => __('Title'),
            'empty' => ''
        ))
        ->addColumn(array(
            'id' => 'created',
            'type' => 'date',
            'title' => __('Created'),
            'width' => '120'
        ))
        ->setDataset($data);
