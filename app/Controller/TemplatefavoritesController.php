<?php

App::uses('AppController', 'Controller');

/**
 * TemplatefavoritesController class of Templatefavorites Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplatefavoritesController extends AppController
{
    /**
     * Initializes components for TemplatefavoritesController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Templatefavorites.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Templatefavorites. Default value is 0.
     * @return void
     */
    public function index($id = 0)
    {
        include('Templatefavorites/index.php');
    }
}