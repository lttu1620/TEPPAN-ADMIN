<?php

$modelName = $this->Template->name;

// process disable/enable
$this->doGeneralAction($modelName);
// change public status
$this->change_public_status($modelName);

// get list mail situation and mail type
$listMailSituation = MasterData::mail_situations_all_key_value();
$listMailType = MasterData::mail_types_all_key_value();

// create breadcrumb
$pageTitle = __('Template list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));
$this->setPageTitle($pageTitle);
// create search form
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'                    => 'mail_type_id',
        'label'                 => __('Mail Type'),
        'options'               => $listMailType,
        'empty'                 => Configure::read('Config.StrChooseOne'),
        'autocomplete_combobox' => true,
    ))
    ->addElement(array(
        'id'                    => 'mail_situation_id',
        'label'                 => __('Mail Situation'),
        'options'               => $listMailSituation,
        'empty'                 => Configure::read('Config.StrChooseOne'),
        'autocomplete_combobox' => true,
    ))
    ->addElement(array(
        'id'    => 'username',
        'label' => __('Username')
    ))
    ->addElement(array(
        'id'    => 'title',
        'label' => __('Title')
    ))
    ->addElement(array(
        'id'      => 'language_type',
        'label'   => __('Language Type'),
        'options' => Configure::read('Config.searchLanguageType')
    ))
    ->addElement(array(
        'id'      => 'is_public',
        'label'   => __('Public'),
        'options' => Configure::read('Config.searchPublic'),
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => Configure::read('Config.StrAll')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'       => __('ID Asc'),
            'id-desc'      => __('ID Desc'),
            'list_no-asc'  => __('List No Asc'),
            'list_no-desc' => __('List No Desc'),
            'title-asc'    => __('Title Asc'),
            'title-desc'   => __('Title Desc'),
            'created-asc'  => __('Created Asc'),
            'created-desc' => __('Created Desc'),
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'       => 'limit',
        'label'    => __('Limit'),
        'options'  => Configure::read('Config.searchPageSize'),
        'onchange' => 'javascript: $(\'#btnSearch\').click();',
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right',
    ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));

$param['mail_type_id'] = $this->getParam('mail_type_id', '');
$param['mail_situation_id'] = $this->getParam('mail_situation_id', '');
$param['username'] = $this->getParam('username', '');
$param['title'] = $this->getParam('title', '');
$param['is_public'] = $this->getParam('is_public', '');
$param['disable'] = $this->getParam('disable', '');
$param['sort'] = $this->getParam('sort', '');

list($total, $data) = Api::call(Configure::read('API.url_templates_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable->addColumn(array(
    'id'    => 'item',
    'name'  => 'items[]',
    'type'  => 'checkbox',
    'value' => '{id}',
    'width' => '20'
))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'width' => '50'
    ))
    ->addColumn(array(
        'id'    => 'list_no',
        'title' => __('List No'),
        'width' => '50'
    ))
    ->addColumn(array(
        'id'    => 'title',
        'type'  => 'link',
        'title' => __('Title'),
        'href'  => '/' . $this->controller . '/update/{id}',
        'empty' => '',
        'width' => 180
    ))
    ->addColumn(array(
        'id'    => 'subject',
        'title' => __('Subject'),
        'empty' => '',
        'width' => 180
    ))
    ->addColumn(array(
        'id'    => 'mail_type_name',
        'type'  => 'link',
        'title' => __('Mail Type'),
        'href'  => '/mailtypes/update/{mail_type_id}',
        'width' => 150
    ))
    ->addColumn(array(
        'id'    => 'mail_situation_name',
        'type'  => 'link',
        'title' => __('Mail Situation'),
        'href'  => '/mailsituations/update/{mail_situation_id}',
        'width' => 150
    ))
    ->addColumn(array(
        'id'    => 'favorite_count',
        'type'  => 'link',
        'title' => __('Favorite count'),
        'href'  => '/templatefavorites/index/{id}',
        'width' => 120
    ))
    ->addColumn(array(
        'id'    => 'username',
        'type'  => 'link',
        'title' => __('Username'),
        'href'  => '/users/profile/{made_by}',
        'empty' => '',
        'width' => 120
    ))
    ->addColumn(array(
        'id'    => 'created',
        'type'  => 'date',
        'title' => __('Created'),
        'width' => 120
    ))
    ->addColumn(array(
        'id'     => 'not_sendable',
        'type'   => 'checkbox',
        'title'  => __('Not Sendable'),
        'toggle' => true,
        'class'  => 'sendable',
        'rules'  => array(
            '1' => $this->AppUI->is_admin ? 'checked' : $this->SimpleTable->yesTemplate(),
            '0' => $this->AppUI->is_admin ? '' : $this->SimpleTable->noTemplate()
        ),
        'empty'  => 0,
        'width'  => 70,
    ))
    ->addColumn(array(
        'id'     => 'not_editable',
        'type'   => 'checkbox',
        'title'  => __('Not Editable'),
        'toggle' => true,
        'class'  => 'editable',
        'rules'  => array(
            '1' => $this->AppUI->is_admin ? 'checked' : $this->SimpleTable->yesTemplate(),
            '0' => $this->AppUI->is_admin ? '' : $this->SimpleTable->noTemplate()
        ),
        'empty'  => 0,
        'width'  => 70,
    ))
    ->addColumn(array(
        'id'     => 'is_public',
        'type'   => 'checkbox',
        'title'  => __('Public'),
        'toggle' => true,
        'class'  => 'public',
        'rules'  => array(
            '1' => $this->AppUI->is_admin ? 'checked' : $this->SimpleTable->yesTemplate(),
            '0' => $this->AppUI->is_admin ? '' : $this->SimpleTable->noTemplate()
        ),
        'empty'  => 0,
        'width'  => 70,
    ))
    ->addColumn(array(
        'id'     => 'disable',
        'type'   => 'checkbox',
        'title'  => __('Status'),
        'toggle' => true,
        'rules'  => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty'  => 0,
        'width'  => 90,
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'title' => array(
            array(
                'field'  => 'title_en',
                'before' => __('English Title') . " : ",
            ),
            array(
                'field'  => 'title_cn',
                'before' => __('Chinese Title') . " : ",
            ),
        )
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Add new'),
        'class' => 'btn btn-primary btn-addnew',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Public'),
        'class' => 'btn btn-primary public',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Private'),
        'class' => 'btn btn-primary private',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id'   => 'action2',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id'   => 'actionId2',
    ))
    ->addHidden(array(
        'type' => 'hidden',
        'id'   => 'optionId2',
    ));
