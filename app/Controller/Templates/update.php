<?php

$modelName = $this->Template->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Template');
if (!empty($id)) {
    // call API get template detail
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_templates_detail'), $param);
    // check available id
    if (empty($data[$modelName])) {
        AppLog::info("Template unavailable", __METHOD__, $param);
        throw new NotFoundException("Template unavailable", __METHOD__, $param);
    }
    $pageTitle = __('Edit template');
}

// get list mail situation and mail type
$listMailSituation = MasterData::mail_situations_all_key_value();
$listMailType = MasterData::mail_types_all_key_value();

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(
        array(
            'name' => $pageTitle,
        )
    );
$this->setPageTitle($pageTitle);
// create update form - Information Form
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(
        array(
            'id'    => 'id',
            'type'  => 'hidden',
            'label' => __('Id'),
        )
    )
    ->addElement(
        array(
            'id'                    => 'language_type',
            'label'                 => __('Language Type'),
            'options'               => Configure::read('Config.updateLanguageType'),
            'autocomplete_combobox' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'list_no',
            'label'    => __('List No'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'                    => 'mail_type_id',
            'label'                 => __('Mail Type'),
            'options'               => $listMailType,
            'empty'                 => Configure::read('Config.StrChooseOne'),
            'autocomplete_combobox' => true,
        )
    )
    ->addElement(
        array(
            'id'                    => 'mail_situation_id',
            'label'                 => __('Mail Situation'),
            'options'               => $listMailSituation,
            'empty'                 => Configure::read('Config.StrChooseOne'),
            'autocomplete_combobox' => true,
        )
    )
    ->addElement(
        array(
            'id'    => 'memo',
            'label' => __('Memo'),
            'rows'  => 4,
        )
    )
    ->addElement(
        array(
            'id'       => 'title',
            'label'    => __('Title'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'       => 'subject',
            'label'    => __('Subject'),
            'required' => true,
        )
    )
    ->addElement(
        array(
            'id'    => 'content',
            'type'  => 'textarea',
            'label' => __('Content'),
            'rows'  => 12,
        )
    )
    ->addElement(
        array(
            'id'    => 'is_public',
            'label' => __('is_public'),
            'type'  => 'hidden',
        )
    )
    ->addElement(
        array(
            'id'    => 'favorite_count',
            'label' => __('favorite_count'),
            'type'  => 'hidden',
        )
    )
    ->addElement(
        array(
            'id'    => 'monthly_used_count',
            'label' => __('monthly_used_count'),
            'type'  => 'hidden',
        )
    )
    ->addElement(
        array(
            'id'    => 'is_angry_saizo',
            'label' => __('is_angry_saizo'),
            'type'  => 'hidden',
        )
    )
    ->addElement(
        array(
            'id'      => 'not_sendable',
            'label'   => __('Not Sendable'),
            'options' => Configure::read('Config.BooleanValue'),
        )
    )
    ->addElement(
        array(
            'id'      => 'not_editable',
            'label'   => __('Not Editable'),
            'options' => Configure::read('Config.BooleanValue'),
        )
    )
    ->addElement(
        array(
            'id'    => 'background_image',
            'type'  => 'file',
            'image' => true,
            'label' => __('Image'),
        )
    )
    ->addElement(
        array(
            'type'  => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        )
    )
    ->addElement(
        array(
            'type'    => 'submit',
            'value'   => __('Cancel'),
            'class'   => 'btn btn-primary pull-left',
            'onclick' => 'return back();',
        )
    );

// process when submit form
if ($this->request->is('post')) {
    $data = $this->getData($modelName);
    if ($model->validateInsertUpdate($data)) {
        if (!empty($_FILES['data'])) {
            foreach ($_FILES['data']['name'][$modelName] as $fieldName => $fieldValue) {
                $filetype = $_FILES['data']['type'][$modelName][$fieldName];
                $filename = $_FILES['data']['name'][$modelName][$fieldName];
                $filedata = $_FILES['data']['tmp_name'][$modelName][$fieldName];
                if (isset($data[$modelName][$fieldName]['remove'])) {
                    $data[$modelName][$fieldName] = '';
                } elseif ($_FILES['data']['error'][$modelName][$fieldName] === 0) {
                    $data[$modelName][$fieldName] = new CurlFile($filedata, $filetype, $filename);
                } elseif (isset($data[$modelName][$fieldName])) {
                    unset($data[$modelName][$fieldName]);
                }
            }
        }
        if ($id = Api::call(Configure::read('API.url_templates_addupdate'), $data[$modelName])) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            if (!empty($id)) {
                $this->redirect("/{$this->controller}/update/{$id}");
            }
            $this->redirect($this->request->here);
        }
    } else {
        AppLog::info("Can not update", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
} else {
    $this->data = $data;
}