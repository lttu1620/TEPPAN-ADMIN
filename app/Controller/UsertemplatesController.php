<?php

App::uses('AppController', 'Controller');

/**
 * UsertemplatesController class of Usertemplates Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UsertemplatesController extends AppController
{
    public $uses = array('UserTemplate');

    /**
     * Initializes components for UsertemplatesController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view update Usertemplates.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Usertemplates. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Usertemplates/update.php');
    }
}
