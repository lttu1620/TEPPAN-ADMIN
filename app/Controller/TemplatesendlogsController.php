<?php

/**
 * TemplatesendlogsController class of Templatesendlogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplatesendlogsController extends AppController
{
    /**
     * Initializes components for TemplatesendlogsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Templatesendlogs.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Templatesendlogs/index.php');
    }

}
