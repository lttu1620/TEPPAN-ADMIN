<?php

$modelName = $this->Mailtype->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Mail type list');
$this->Breadcrumb->setTitle($pageTitle)
	->add(array(
		'name' => $pageTitle,
	));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
	->setModelName($modelName)
	->setAttribute('type', 'get')
	->addElement(array(
		'id'    => 'name',
		'type'  => 'text',
		'label' => __('Name'),
	))
	->addElement(array(
		'id'    => 'name_en',
		'type'  => 'text',
		'label' => __('English Name'),
	))
	->addElement(array(
		'id'    => 'name_cn',
		'type'  => 'text',
		'label' => __('Chinese Name'),
	))
	->addElement(array(
		'id'      => 'disable',
		'label'   => __('Status'),
		'options' => Configure::read('Config.searchStatus'),
		'empty'   => Configure::read('Config.StrAll')
	))
	->addElement(array(
		'id'      => 'sort',
		'label'   => __('Sort'),
		'options' => array(
			'name-asc'     => __('Name Asc'),
			'name-desc'    => __('Name Desc'),
			'name_en-asc'  => __('English Name Asc'),
			'name_en-desc' => __('English Name Desc'),
			'name_cn-asc'  => __('Chinese Name Asc'),
			'name_cn-desc' => __('Chinese Name Desc'),
			'created-asc'  => __('Created Asc'),
			'created-desc' => __('Created Desc'),
		),
		'empty'   => Configure::read('Config.StrChooseOne'),
	))
	->addElement(array(
		'id'      => 'limit',
		'label'   => __('Limit'),
		'options' => Configure::read('Config.searchPageSize'),
	))
	->addElement(array(
		'type'  => 'submit',
		'value' => __('Search'),
		'class' => 'btn btn-primary pull-right'
	));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if (!$this->AppUI->is_admin) {
	$param['user_id'] = $this->AppUI->id;
}
list($total, $data) = Api::call(Configure::read('API.url_mailtypes_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable->addColumn(array(
	'id'    => 'item',
	'name'  => 'items[]',
	'type'  => 'checkbox',
	'value' => '{id}',
	'width' => 20
))
	->addColumn(array(
		'id'    => 'id',
		'type'  => 'link',
		'title' => __('ID'),
		'href'  => '/' . $this->controller . '/update/{id}',
		'width' => 50
	))
	->addColumn(array(
		'id'    => 'image_url',
		'title' => __('Image'),
		'width' => '100'
	))
	->addColumn(array(
		'id'    => 'name',
		'type'  => 'link',
		'title' => __('Name'),
		'href'  => '/' . $this->controller . '/update/{id}',
		'width' => 350
	))
	->addColumn(array(
		'id'    => 'name_en',
		'type'  => 'link',
		'title' => __('English Name'),
		'href'  => '/' . $this->controller . '/update/{id}',
		'hidden' => true
	))
	->addColumn(array(
		'id'    => 'name_cn',
		'type'  => 'link',
		'title' => __('Chinese Name'),
		'href'  => '/' . $this->controller . '/update/{id}',
		'hidden' => true
	))
	->addColumn(array(
		'id'    => 'template_count',
		'type'  => 'link',
		'title' => __('Template Count'),
		'href'  => '/templates?mail_type_id={id}&language_type=all',
		'width' => 120
	))
	->addColumn(array(
		'id'    => 'template_count_jp',
		'type'  => 'link',
		'title' => __('Japanese Template Count'),
		'href'  => '/templates?mail_type_id={id}&language_type=1',
		'width' => 120
	))
	->addColumn(array(
		'id'    => 'template_count_en',
		'type'  => 'link',
		'title' => __('English Template Count'),
		'href'  => '/templates?mail_type_id={id}&language_type=2',
		'width' => 120
	))
	->addColumn(array(
		'id'    => 'template_count_cn',
		'type'  => 'link',
		'title' => __('Chinese Template Count'),
		'href'  => '/templates?mail_type_id={id}&language_type=3',
		'width' => 120
	))
	->addColumn(array(
		'id'    => 'created',
		'title' => __('Created'),
		'type'  => 'date',
		'width' => 150
	))
	->addColumn(array(
		'id' => 'disable',
		'type' => 'checkbox',
		'title' => __('Status'),
		'toggle' => true,
		'rules' => array(
			'0' => 'checked',
			'1' => ''
		),
		'empty' => 0,
		'width' => 90,
	))
	->setDataset($data)
	->setMergeColumn(array(
		'name' => array(
			array(
				'field' => 'name_en',
				'before' => __('English Name'). " : ",
			),
			array(
				'field' => 'name_cn',
				'before' => __('Chinese Name'). " : ",
			),
		)
	))
	->addButton(array(
		'type'  => 'submit',
		'value' => __('Add new'),
		'class' => 'btn btn-primary btn-addnew',
	))
	->addButton(array(
		'id'    => 'btn-disable',
		'type'  => 'submit',
		'value' => __('Disable'),
		'class' => 'btn btn-primary btn-disable',
	))
	->addButton(array(
		'id'    => 'btn-enable',
		'type'  => 'submit',
		'value' => __('Enable'),
		'class' => 'btn btn-primary btn-enable',
	));
