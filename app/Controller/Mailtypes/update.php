<?php

$modelName = $this->Mailtype->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Mail Type');
if (!empty($id)) {
	$param['id'] = $id;
	$data[$modelName] = Api::call(Configure::read('API.url_mailtypes_detail'), $param);
	if (empty($data[$modelName])) {
		AppLog::info("Mail Type unavailable", __METHOD__, $param);
		throw new NotFoundException("Mail Type unavailable", __METHOD__, $param);
	}
	$pageTitle = __('Edit Mail Type');
}

// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
	->add(array(
		'name' => $pageTitle
	));
$this->setPageTitle($pageTitle);
// create Update form 
$this->UpdateForm->setModelName($modelName)
	->setData($data)
	->addElement(array(
		'id'    => 'id',
		'type'  => 'hidden',
		'label' => __('id'),
	))
	->addElement(array(
		'id'    => 'name',
		'label' => __('Name')
	))
	->addElement(array(
		'id'    => 'name_en',
		'label' => __('English Name')
	))->addElement(array(
		'id'    => 'name_cn',
		'label' => __('Chinese Name')
	))
	->addElement(array(
		'id' => 'image_url',
		'label' => __('Image')
	))
	->addElement(array(
		'type'  => 'submit',
		'value' => __('Save'),
		'class' => 'btn btn-primary pull-left',
	))
	->addElement(array(
		'type'    => 'submit',
		'value'   => __('Cancel'),
		'class'   => 'btn btn-primary pull-left',
		'onclick' => 'return back();'
	));

if ($this->request->is('post')) {
	$data = $this->getData($modelName);
	if ($model->validateInsertUpdate($data)) {
		$id = Api::call(Configure::read('API.url_mailtypes_addupdate'), $data[$modelName]);
		if (Api::getError()) {
			AppLog::info("Can not update mail type", __METHOD__, Api::getError());
			return $this->Common->setFlashErrorMessage(Api::getError());
		} else {
			$this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
			$this->redirect($this->request->here(false));
		}
	} else {
		$this->Common->setFlashErrorMessage($model->validationErrors);
	}
}