<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin && $id <= 0) {
    $param['id'] = $this->AppUI->id;
} else
    $param['id'] = $id == 0 ? -1 : $id;
$param['full'] = true;
$data[$modelName] = Api::Call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());

if (!$this->AppUI->is_admin) {
    if ($this->AppUI->company_id != $data[$modelName]['company_id']) {
        AppLog::info("User not same company", __METHOD__, $data[$modelName]['company_id']);
        throw new NotFoundException("User not same company", __METHOD__, $data[$modelName]['company_id']);
    }
}
$pageTitle = __('Update user facebook information');

// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));
// Create tab for setting Profile
$Profiletab[] = array(
    'name' => __('User information'),
    'link' => $this->request->base . "/users/profileinformation/" . ($this->AppUI->is_admin == 1 || $id > 0 ? "{$id}" : ""),
);
if (!empty($data[$modelName]['facebook_id'])) {
    $Profiletab[] = array(
        'name' => __('Facebook profile'),
        'class' => 'active'
    );
}
if (!empty($data[$modelName]['is_company']) && !empty($data[$modelName]['company_id'])) {

    $Profiletab[] = array(
        'name' => __('Company profile'),
        'link' => $this->request->base . "/users/companyinformation/" . ($this->AppUI->is_admin == 1 || $id > 0 ? "{$data[$modelName]['company_id']}/{$id}" : "{$data[$modelName]['company_id']}"),
    );
}
$this->set('profileTab', $this->Common->renderProfileTab($Profiletab));

$this->UpdateForm
    ->setData($data)
    ->setModelName($modelName)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'user_id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'facebook_id',
        'type' => 'text',
        'readonly' => true,
        'label' => __('Facebook ID')
    ))
    ->addElement(array(
        'id' => 'facebook_username',
        'type' => 'text',
        'readonly' => true,
        'label' => __('Facebook username')
    ))
    ->addElement(array(
        'id' => 'facebook_email',
        'type' => 'text',
        'readonly' => true,
        'label' => __('facebook email')
    ))
    ->addElement(array(
        'id' => 'facebook_image',
        'readonly' => true,
        'image' => true,
        'title' => __('Facebook image'),
    ))
    ->addElement(array(
        'id' => 'facebook_gender',
        'type' => 'text',
        'label' => __('Facebook gender'),
        'readonly' => true,
    ))
    ->addElement(array(
        'id' => 'facebook_name',
        'type' => 'text',
        'label' => __('Facebook name')
    ))
    ->addElement(array(
        'id' => 'facebook_first_name',
        'type' => 'text',
        'label' => __('Facebook first name')
    ))
    ->addElement(array(
        'id' => 'facebook_last_name',
        'type' => 'text',
        'label' => __('facebook last name')
    ))
    ->addElement(array(
        'id' => 'facebook_link',
        'type' => 'text',
        'label' => __('Facebook link'),
        'readonly' => true,
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));
// Process when submit form
if ($this->request->is('post')) {
    // if case add new User
    $data = $this->getData($modelName);
    $valiadateMode = false;
    unset($data[$modelName]['facebook_image']);
    unset($data[$modelName]['facebook_email']);
    $id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName]);
    if (!empty($id) && !Api::getError()) {
        $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
        return $this->redirect("/{$this->controller}/facebookinformation/{$id}");
    }
    // if validation error from api, write log and set validation error
    AppLog::info("Can not update", __METHOD__, $this->data);
    $model->setValidationErrors(Api::getError());

    // show validation error    
    $this->Common->setFlashErrorMessage($model->validationErrors);
}