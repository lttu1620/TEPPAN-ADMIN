<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin) {
    $user_id = $this->AppUI->id;
}
$param['id'] = $user_id;
$param['full'] = true;
$usersDetail = Api::Call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($usersDetail));

// create breadcrumb
$pageTitle = __('Favorites templates');
$this->Breadcrumb->setTitle($pageTitle)
        ->add(array(
            'name' => $pageTitle,
        ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['user_id'] = $user_id;
list($total, $data) = Api::call(Configure::read('API.url_templatefavorites_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
	->addColumn(array(
		'id' => 'title',
		'title' => __('Title'),
		'width' => '200'
	))
        ->setDataset($data);

