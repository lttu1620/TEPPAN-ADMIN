<?php

$modelName = $this->User->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Signatures list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['user_id'] = $id;
$usersDetail = Api::Call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($usersDetail));
$data = Api::call(Configure::read('API.url_signatures_list'), $param);
$this->Common->handleException(Api::getError());
$this->SimpleTable
    ->addColumn(array(
        'id' => 'language_name',
        'title' => __('Language'),
        'empty' => '',
    ))
    ->addColumn(array(
        'id' => 'content',
        'title' => __('Content'),
        'empty' => '',
    ))
    ->addColumn(array(
        'id' => 'created',
        'title' => __('Date'),
        'type' => 'date',
        'width' => 120
    ))
    ->setDataset($data);
