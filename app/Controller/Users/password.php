<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();

// Create breadcrumb 
$pageTitle = __('Change password');
$this->Breadcrumb->SetTitle($pageTitle)        
        ->add(array(      
            'link' => $this->request->base.'/users',
            'name' => __('User list')
        ))
        ->add(array(
            'name' => $pageTitle
        ));

// Create Update form user
$this->UpdateForm
        ->setModelName($modelName)
        //Create form for addupdate user
        ->addElement(array(
            'id' => 'password',
            'type' => 'password',
            'label' => __('New password'),
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'id' => 'password_confirm',
            'type' => 'password',
            'label' => __('Confirm password'),
            'autocomplete' => 'off'
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Save'),
            'class' => 'btn btn-primary pull-left',
        ))
        ->addElement(array(
            'type' => 'submit',
            'value' => __('Cancel'),
            'class' => 'btn btn-primary pull-left',
            'onclick' => 'return back();'
        ));

if ($this->request->is('post')) {
    if ($model->validate_passwords_user_profile($this->getData($modelName))) {
        $param = array(
            'user_id' => $id,
            'password' => $model->data[$modelName]['password']
        );
        if (Api::call(Configure::read('API.url_userprofiles_changepassword'), $param)) {
            $this->Common->setFlashSuccessMessage(__('Password changed successfuly'));
            $this->redirect($this->request->here(false));
        }
    } else {
        AppLog::info("Can not update password", __METHOD__, $this->data);
        $this->Common->setFlashErrorMessage($model->validationErrors);
    }
}