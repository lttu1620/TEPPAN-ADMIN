<?php

$modelName = $this->User->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create search conditions
$listUniversity = MasterData::universities_all_key_value();
$listCampus = array();
$listDepartment = array();

// If has university_id==> call ajax get data campus
if (isset($data[$modelName]['university_id'])) {
    try {
        $lstData = Api::call(Configure::read('API.url_campuses_list'), array(
                'university_id' => $data[$modelName]['university_id'])
        );
        foreach ($lstData as $item) {
            $listCampus[$item['id']] = (string)$item['name'];
        }

        // Get data for department
        $lstData = Api::call(Configure::read('API.url_departments_list'), array(
                'university_id' => $data[$modelName]['university_id'])
        );
        foreach ($lstData as $item) {
            $listDepartment[$item['id']] = (string)$item['name'];
        }
    } catch (Exception $exc) {
        //echo $exc->getTraceAsString();
    }
}

// create breadcrumb
$pageTitle = __('User list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

// Create search form 
$this->SearchForm->setModelName($modelName)
    ->setAttribute('type', 'get')
    ->addElement(array(
        'id'    => 'name',
        'label' => __('Username')
    ))
    ->addElement(array(
        'id'    => 'mail',
        'label' => __('Mail')
    ))
    ->addElement(array(
        'id' => 'university_id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'university_name',
        'label' => __('University'),
        'options' => array(
            'url' => "/ajax/autocompleteuniversity",
            'callback' => "callbackUserProfile",
        ),
        'autocomplete_ajax' => true,
        'onblur'=>"onblurUniversity(this,'university_id');"
    ))
    ->addElement(array(
        'id' => 'campus_id',
        'label' => __('Campus'),
        'options' => $listCampus,
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'department_id',
        'label' => __('Department'),
        'options' => $listDepartment,
        'empty' => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id' => 'department_name',
        'label' => __('Department Name'),
    ))
    ->addElement(array(
        'id'      => 'sex_id',
        'label'   => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty'   => Configure::read('Config.StrAll'),
    ))
    ->addElement(array(
        'id'    => 'tel',
        'label' => __('Tel')
    ))
    ->addElement(array(
        'id'      => 'high_graduate_year',
        'label'   => __('High Graduate Year'),
        'options' => Configure::read('Config.searchYear'),
        'empty'   => '',
    ))
    ->addElement(array(
        'id'      => 'univ_graduate_year',
        'label'   => __('Univ Graduate Year'),
        'options' => Configure::read('Config.searchYear'),
        'empty'   => '',
    ))
    ->addElement(array(
        'id'      => 'disable',
        'label'   => __('Status'),
        'options' => Configure::read('Config.searchStatus'),
        'empty'   => __('All')
    ))
    ->addElement(array(
        'id'      => 'sort',
        'label'   => __('Sort'),
        'options' => array(
            'id-asc'       => 'ID Asc',
            'id-desc'      => 'ID Desc',
            'name-asc'     => 'Name Asc',
            'name-desc'    => 'Name Desc',
            'mail-asc'    => 'Mail Asc',
            'mail-desc'   => 'Mail Desc',
            'created-asc'  => 'Created Asc',
            'created-desc' => 'Created Desc',
        ),
        'empty'   => Configure::read('Config.StrChooseOne'),
    ))
    ->addElement(array(
        'id'      => 'limit',
        'label'   => __('Limit'),
        'options' => Configure::read('Config.searchPageSize'),
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Search'),
        'class' => 'btn btn-primary pull-right'
    ));
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));

list($total, $data) = Api::call(Configure::read('API.url_users_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'item',
        'name'  => 'items[]',
        'type'  => 'checkbox',
        'value' => '{id}',
        'width' => '20'
    ))
    ->addColumn(array(
        'id'    => 'id',
        'type'  => 'link',
        'title' => __('ID'),
        'href'  => '/' . $this->controller . '/profile/{id}',
        'width' => '30'
    ))
    ->addColumn(array(
        'id'    => 'image',
        'type'  => 'image',
        'title' => __('Image'),
        'src'   => '{image}',
        'width' => '50'
    ))
    ->addColumn(array(
        'id'    => 'name',
        'type'  => 'link',
        'href'  => '/' . $this->controller . '/profile/{id}',
        'title' => __('Username'),
        'width' => 150,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'is_ios',
        'title' => '端末',
        'width' => 150,
        'empty' => '',
        'rules'  => array(
            '0' => '',
            '1' => '<div class="mt5"><i class="fa fa-apple"></i></div>'
        ),
    ))
    ->addColumn(array(
        'id'    => 'is_android',
        'title' => __('Android'),
        'empty' => '',
        'rules'  => array(
            '0' => '',
            '1' => '<i class="fa fa-android"></i>'
        ),
        'hidden'=> true
    ))
    ->addColumn(array(
        'id'     => 'sex_id',
        'title'  => __('Gender'),
        'rules'  => Configure::read('Config.searchGender'),
        'empty'  => '',
        'width'  => 80,
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'mail',
        'title'  => __('Mail'),
        'empty'  => '',
        'width'  => 200,
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'university_name',
        'title' => __('University'),
        'width' => 200,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'campus_name',
        'title' => __('Campus'),
        'width' => 200,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'    => 'department_name',
        'title' => __('Department'),
        'width' => 200,
        'empty' => ''
    ))
    ->addColumn(array(
        'id'     => 'created',
        'type'   => 'date',
        'title'  => __('Created'),
        'width'  => 120,
        'hidden' => true
    ))
    ->addColumn(array(
        'id' => 'disable',
        'type' => 'checkbox',
        'title' => __('Status'),
        'toggle' => true,
        'rules' => array(
            '0' => 'checked',
            '1' => ''
        ),
        'empty' => 0,
        'width' => 90,
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'name' => array(
            array(
                'field' => 'sex_id',
                'before' => __('Gender'). " : ",
            ),
            array(
                'field' => 'mail',
                'before' => __('Mail'). " : ",
            ),
            array(
                'field' => 'created',
                'before' => __('Created'). " : ",
            ),
        )
    ))
     ->setMergeColumn(array(
        'is_ios' => array(
            array(
                'field'  => 'is_android',
            )
        )
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Disable'),
        'class' => 'btn btn-primary btn-disable',
    ))
    ->addButton(array(
        'type'  => 'submit',
        'value' => __('Enable'),
        'class' => 'btn btn-primary btn-enable',
    ));
