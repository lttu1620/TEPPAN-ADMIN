<?php

$modelName = $this->User->name;

// create breadcrumb
$pageTitle = __('Template list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

$userInfo = Api::Call(Configure::read('API.url_templates_list'), array('id' => $user_id, 'full' => 1), false, array());
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($userInfo));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
list($total, $data) = Api::call(Configure::read('API.url_user_templates_list'), array('user_id' => $user_id, false, array(0, array())));
$this->Common->handleException(Api::getError());

$this->set('total', $total);
$this->set('limit', $param['limit']);
$this->SimpleTable
    ->addColumn(array(
        'id'    => 'title',
        'type'  =>'link',
        'title' => __('Title'),
        'href'  => '/usertemplates/update/{user_template_id}',
        'width' => '250'
    ))
    ->addColumn(array(
        'id'    => 'subject',
        'title' => __('Subject'),
        'width' => 350,
        'empty' => '',
    ))
    ->addColumn(array(
        'id'    => 'created',
        'title' => __('Created'),
        'type'  => 'date',
        'empty' => '',
        'width' => '120'
    ))
    ->setDataset($data);

