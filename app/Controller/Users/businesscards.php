<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin) {
    $user_id = $this->AppUI->id;
}
$param['id'] = $user_id;
$param['full'] = true;
$usersDetail = Api::Call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($usersDetail));

// create breadcrumb
$pageTitle = __('Business card list');
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle,
    ));

// create data table
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['user_id'] = $user_id;

list($total, $data) = Api::call(Configure::read('API.url_businesscards_list'), $param, false, array(0, array()));
$this->Common->handleException(Api::getError());
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable->addColumn(array(
    'id'    => 'image',
    'type'  => 'image',
    'title' => __('Image'),
    'src'   => '{image}',
    'width' => '60'
))
    ->addColumn(array(
        'id'    => 'name',
        'type'  => 'link',
        'href'  => '/businesscards/update/{id}',
        'title' => __('Name'),
        'empty' => '',
        'width' => '500'
    ))
    ->addColumn(array(
        'id'     => 'phone',
        'title'  => __('Phone'),
        'empty'  => '',
        'width'  => '200',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'     => 'email',
        'title'  => __('Email'),
        'empty'  => '',
        'width'  => '200',
        'hidden' => true
    ))
    ->addColumn(array(
        'id'    => 'company',
        'title' => __('Company'),
        'empty' => '',
    ))
    ->setDataset($data)
    ->setMergeColumn(array(
        'name' => array(
            array(
                'field'  => 'phone',
                'before' => __('Phone') . " : ",
            ),
            array(
                'field'  => 'email',
                'before' => __('Email') . " : ",
            ),
        )
    ));