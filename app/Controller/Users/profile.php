<?php

$modelName = $this->User->name;
$model = $this->{$modelName};
$data = array();
if (!$this->AppUI->is_admin) {
    $id = $this->AppUI->id;
}
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
$param['id'] = $id;
$data[$modelName] = Api::Call(Configure::read('API.url_users_detail'), $param);
$this->Common->handleException(Api::getError());
$this->set('profileTab', $this->Common->renderProfileTab($data[$modelName]));

$pageTitle = __('Update user profile');

$listUniversity = MasterData::universities_all_key_value();
$listCampus = array();
$listDepartment = array();

// If has university_id==> call ajax get data campus
if (!empty($data[$modelName]['university_id'])) {
    $lstData = Api::call(Configure::read('API.url_campuses_list'), array(
            'university_id' => $data[$modelName]['university_id'])
    );
    foreach ($lstData as $item) {
        $listCampus[$item['id']] = (string)$item['name'];
    }
    // Get data for department
    $lstData = Api::call(Configure::read('API.url_departments_list'), array(
            'university_id' => $data[$modelName]['university_id'])
    );
    foreach ($lstData as $item) {
        $listDepartment[$item['id']] = (string)$item['name'];
    }
}

// Create breadcrumb 
$this->Breadcrumb->SetTitle($pageTitle)
    ->add(array(
        'name' => $pageTitle
    ));

// Create Update form user
$userForm = $this->UpdateForm->reset()
    ->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id'    => 'id',
        'type'  => 'hidden',
        'label' => __('id'),
    ))
    ->addElement(array(
        'id'    => 'language_type',
        'type'  => 'hidden',
        'label' => __('Language Type'),
    ))
    ->addElement(array(
        'id'    => 'submitType',
        'type'  => 'hidden',
        'value' => 'UserProfile'
    ))
    ->addElement(array(
        'id'           => 'name',
        'label'        => __('Name'),
        'autocomplete' => 'off',
        'required'     => true
    ))
    ->addElement(array(
        'id'    => 'first_name',
        'label' => __('First Name'),
    ))
    ->addElement(array(
        'id'    => 'last_name',
        'label' => __('Last Name'),
    ))
    ->addElement(array(
        'id'    => 'first_name_kana',
        'label' => __('First Name (Kana)'),
    ))
    ->addElement(array(
        'id'    => 'last_name_kana',
        'label' => __('Last Name (Kana)'),
    ))
    ->addElement(array(
        'id'    => 'image',
        'type'  => 'file',
        'image' => true,
        'label' => __('Image')
    ))
    ->addElement(array(
        'id'   => 'university_id',
        'type' => 'hidden',
    ));
if (!empty($data[$modelName]['university_id'])) {
    $userForm = $userForm->addElement(array(
        'id'                => 'university_name',
        'label'             => __('University'),
        'options'           => array(
            'url'      => Router::url("/ajax/autocompleteuniversity"),
            'callback' => "callbackUserProfile",
        ),
        'autocomplete_ajax' => true,
        'onblur'            => "onblurUniversity(this,'university_id');"
    ))
        ->addElement(array(
            'id'      => 'campus_id',
            'label'   => __('Campus'),
            'options' => $listCampus,
            'empty'   => Configure::read('Config.StrChooseOne'),
        ))
        ->addElement(array(
            'id'      => 'department_id',
            'label'   => __('Department'),
            'options' => $listDepartment,
            'empty'   => Configure::read('Config.StrChooseOne'),
        ));
} else {
    $userForm = $userForm->addElement(array(
        'id'    => 'university_name',
        'label' => __('University')
    ))
        ->addElement(array(
            'id'    => 'department_name',
            'label' => __('Department')
        ));
}
$userForm = $userForm->addElement(array(
    'id'    => 'major',
    'label' => __('Major'),
))
    ->addElement(array(
        'id'    => 'course',
        'label' => __('Course'),
    ))
    ->addElement(array(
        'id'      => 'sex_id',
        'label'   => __('Gender'),
        'options' => Configure::read('Config.searchGender'),
        'empty'   => Configure::read('Config.StrChooseOne')
    ))
    ->addElement(array(
        'id'    => 'postcode',
        'label' => __('Postcode'),
    ))
    ->addElement(array(
        'id'    => 'prefecture_id',
        'label' => __('Prefecture Id'),
        'type'  => 'text'
    ))
    ->addElement(array(
        'id'    => 'city',
        'label' => __('City'),
    ))
    ->addElement(array(
        'id'    => 'address',
        'label' => __('Address'),
    ))
    ->addElement(array(
        'id'    => 'tel',
        'label' => __('Tel'),
    ))
    ->addElement(array(
        'id'    => 'tel_mobile',
        'label' => __('Tel Mobile'),
    ))
    ->addElement(array(
        'id'    => 'mail',
        'label' => __('Mail PC'),
    ))
    ->addElement(array(
        'id'    => 'mail_mobile',
        'label' => __('Mail Mobile'),
    ))
    ->addElement(array(
        'id'    => 'signature',
        'type'  => 'textarea',
        'rows'  => '10',
        'label' => __('Signature'),
    ))
    ->addElement(array(
        'id'      => 'high_graduate_year',
        'label'   => __('High Graduate Year'),
        'options' => Configure::read('Config.searchYear'),
        'empty'   => '',
    ))
    ->addElement(array(
        'id'      => 'univ_graduate_year',
        'label'   => __('Univ Graduate Year'),
        'options' => Configure::read('Config.searchYear'),
        'empty'   => '',
    ))
    ->addElement(array(
        'id'    => 'device_id',
        'label' => __('Device Id'),
        'type'  => 'text'
    ))
    ->addElement(array(
        'type'  => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ));
$this->set('userForm', $userForm->get());

if ($this->request->is('post')) {
    // if case add new User
    $data = $this->getData($modelName);
    $submitType = $data[$modelName]['submitType'];
    unset($data[$modelName]['submitType']);
    $validateMode = false;
    switch ($submitType) {
        case "UserProfile":
            $validateMode = $model->validateUserInsertUpdate($data);
            if ($validateMode) {
                // Processing upload Image
                if (!empty($_FILES['data']['name'][$modelName]['image'])) {
                    $image_url = $this->Image->uploadImage("{$modelName}.image");
                    $data[$modelName]['image'] = $image_url;
                } else {
                    unset($data[$modelName]['image']);
                }
                unset($data[$modelName]['password_confirm']);
                unset($data[$modelName]['app_id']);
                unset($data[$modelName]['email']);
                if ($id = Api::call(Configure::read('API.url_users_addupdate'), $data[$modelName])) {
                    if (Api::getError()) {
                        return $this->Common->setFlashErrorMessage(Api::getError());
                    } else {
                        $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
                    }
                } else {
                    AppLog::info("Can not update", __METHOD__, $this->data);
                    $this->Common->setFlashErrorMessage(__('Data saved unsuccessfuly'));
                }
            } else {
                $this->Common->setFlashErrorMessage($model->validationErrors);
            }
            break;
    }
    $this->redirect($this->request->here(false));
} 