<?php

App::uses('AppController', 'Controller');

/**
 * MailtypesController class of Mailtypes Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class MailtypesController extends AppController
{
    /**
     * Initializes components for MailtypesController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Mailtypes.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Mailtypes/index.php');
    }

    /**
     * Handles user interaction of view update Mailtypes.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Mailtypes. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Mailtypes/update.php');
    }
}
