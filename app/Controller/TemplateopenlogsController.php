<?php

/**
 * TemplateopenlogsController class of Templateopenlogs Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplateopenlogsController extends AppController
{
    /**
     * Initializes components for TemplateopenlogsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Templateopenlogs.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Templateopenlogs/index.php');
    }

}
