<?php

if($isPrevUrl){// to back to the previous page.
   $result = AppCache::read('pre_businesscards_url');
   if($result === false){
       AppCache::write('pre_businesscards_url', $this->referer(), 60 * 60);
   } 
}

$modelName = $this->Businesscard->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add Business Card');
if (!empty($id)) {
	$param['id'] = $id;
	$data[$modelName] = Api::call(Configure::read('API.url_businesscards_detail'), $param);
	if (empty($data[$modelName])) {
		AppLog::info("Business Card unavailable", __METHOD__, $param);
		throw new NotFoundException("Business Card unavailable", __METHOD__, $param);
	}
	$pageTitle = __('Edit Business Card');
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
	->add(array(
		'name' => $pageTitle
	));

// create Update form 
$this->UpdateForm->setModelName($modelName)
	->setData($data)
	->addElement(array(
		'id'    => 'id',
		'type'  => 'hidden',
		'label' => __('id'),
	))
	->addElement(array(
		'id'    => 'user_id',
		'label' => __('User Id'),
		'type'  => 'text'
	))
	->addElement(array(
		'id'    => 'image',
		'label' => __('Image')
	))
	->addElement(array(
		'id'    => 'company',
		'label' => __('Company')
	))
	->addElement(array(
		'id'    => 'name',
		'label' => __('Name')
	))
	->addElement(array(
		'id'    => 'phone',
		'label' => __('Phone')
	))->addElement(array(
		'id'    => 'email',
		'label' => __('Email')
	))
	->addElement(array(
		'type'  => 'submit',
		'value' => __('Save'),
		'class' => 'btn btn-primary pull-left',
	))
	->addElement(array(
		'type'    => 'submit',
		'value'   => __('Cancel'),
		'class'   => 'btn btn-primary pull-left',
		'onclick' => 'return back();'
	));

if ($this->request->is('post')) {
	$data = $this->getData($modelName);
	if ($model->validateInsertUpdate($data)) {
		$id = Api::call(Configure::read('API.url_businesscards_addupdate'), $data[$modelName]);
		if (Api::getError()) {
			AppLog::info("Can not update business card", __METHOD__, Api::getError());
			return $this->Common->setFlashErrorMessage(Api::getError());
		} else {
			$this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
			if($isPrevUrl){
			    $pre_url = AppCache::read('pre_businesscards_url');
			    AppCache::delete('pre_businesscards_url');
			    $this->redirect($pre_url);
			}else{
			    $this->redirect($this->request->here(false));
			}
		}
	} else {
		$this->Common->setFlashErrorMessage($model->validationErrors);
	}
}