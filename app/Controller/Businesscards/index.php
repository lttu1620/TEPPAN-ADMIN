<?php

$modelName = $this->Businesscard->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Business Card List');
$this->Breadcrumb->setTitle($pageTitle)
	->add(array(
		'name' => $pageTitle,
	));
$this->setPageTitle($pageTitle);
// Create search form 
$this->SearchForm
	->setModelName($modelName)
	->setAttribute('type', 'get')
	->addElement(array(
		'id'    => 'username',
		'type'  => 'text',
		'label' => __('Username'),
	))
	->addElement(array(
		'id'       => 'name',
		'type'     => 'text',
		'label'    => __('Name'),
		'required' => true
	))
	->addElement(array(
		'id'    => 'company',
		'type'  => 'text',
		'label' => __('Company'),
	))
	->addElement(array(
		'id'    => 'phone',
		'type'  => 'text',
		'label' => __('Phone'),
	))
	->addElement(array(
		'id'    => 'email',
		'type'  => 'text',
		'label' => __('Email'),
	))
	->addElement(array(
		'id'      => 'disable',
		'label'   => __('Status'),
		'options' => Configure::read('Config.searchStatus'),
		'empty'   => Configure::read('Config.StrAll')
	))
	->addElement(array(
		'id'      => 'sort',
		'label'   => __('Sort'),
		'options' => array(
			'name-asc'     => __('Name Asc'),
			'name-desc'    => __('Name Desc'),
			'created-asc'  => __('Created Asc'),
			'created-desc' => __('Created Desc')
		),
		'empty'   => Configure::read('Config.StrChooseOne'),
	))
	->addElement(array(
		'id'      => 'limit',
		'label'   => __('Limit'),
		'options' => Configure::read('Config.searchPageSize'),
	))
	->addElement(array(
		'type'  => 'submit',
		'value' => __('Search'),
		'class' => 'btn btn-primary pull-right'
	));

// call api to query data
$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if (!$this->AppUI->is_admin) {
	$param['user_id'] = $this->AppUI->id;
}
list($total, $data) = Api::call(Configure::read('API.url_businesscards_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

// create data table
$this->SimpleTable->addColumn(array(
	'id'    => 'item',
	'name'  => 'items[]',
	'type'  => 'checkbox',
	'value' => '{id}',
	'width' => 20
))
	->addColumn(array(
		'id'    => 'id',
		'type'  => 'link',
		'title' => __('ID'),
		'href'  => '/' . $this->controller . '/update/{id}',
		'width' => 50
	))
	->addColumn(array(
		'id'    => 'image',
		'title' => __('Image'),
		'width' => '100',
		'empty' => ''
	))
	->addColumn(array(
		'id'    => 'username',
		'type'  => 'link',
		'title' => __('Username'),
		'href'  => '/users/profile/{user_id}',
		'width' => 350,
		'empty' => ''
	))
	->addColumn(array(
		'id'    => 'name',
		'type'  => 'link',
		'title' => __('Name'),
		'href'  => '/' . $this->controller . '/update/{id}',
		'width' => 350
	))

	->addColumn(array(
		'id'    => 'email',
		'title' => __('Email'),
		'empty' => ''
	))
	->addColumn(array(
		'id'    => 'company',
		'title' => __('Company'),
		'empty' => ''
	))
	->addColumn(array(
		'id'    => 'phone',
		'title' => __('Phone'),
		'empty' => ''
	))
	->addColumn(array(
		'id'    => 'created',
		'title' => __('Created'),
		'type'  => 'date',
		'width' => 150
	))
	->addColumn(array(
		'id' => 'disable',
		'type' => 'checkbox',
		'title' => __('Status'),
		'toggle' => true,
		'rules' => array(
			'0' => 'checked',
			'1' => ''
		),
		'empty' => 0,
		'width' => 90,
	))
	->setDataset($data)
	->addButton(array(
		'type'  => 'submit',
		'value' => __('Add new'),
		'class' => 'btn btn-primary btn-addnew',
	))
	->addButton(array(
		'id'    => 'btn-disable',
		'type'  => 'submit',
		'value' => __('Disable'),
		'class' => 'btn btn-primary btn-disable',
	))
	->addButton(array(
		'id'    => 'btn-enable',
		'type'  => 'submit',
		'value' => __('Enable'),
		'class' => 'btn btn-primary btn-enable',
	));
