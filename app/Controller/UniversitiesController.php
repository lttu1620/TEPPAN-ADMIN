<?php

App::uses('AppController', 'Controller');

/**
 * UniversitiesController class of Universities Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UniversitiesController extends AppController
{
    /**
     * Initializes components for UniversitiesController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Universities.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Universities/index.php');
    }

    /**
     * Handles user interaction of view update Universities.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Universities. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Universities/update.php');
    }

}
