<?php

App::uses('AppController', 'Controller');

/**
 * TemplatesController class of Templates Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TemplatesController extends AppController
{
    /**
     * Initializes components for TemplatesController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Templates.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Templates/index.php');
    }

    /**
     * Handles user interaction of view update Templates.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Templates. Default value is 0.
     * @return void
     */
    public function update($id = 0)
    {
        include('Templates/update.php');
    }
    
    /**
     * Change public status
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function change_public_status()
    {
        $data = $this->request->data;
        if ($this->request->is('post')) {
            if (!empty($data['actionId2'])) {
                $data['items'] = array($data['actionId2']);
            }
            if (!empty($data['action2']) && !empty($data['items'])) {
                $param['id'] = implode(',', $data['items']);
                $param['is_public'] = $data['action2'] == 'public' ? '1' : '0';
                if (!Api::call(Configure::read('API.url_templates_ispublic'), $param)) {
                    AppLog::warning("Can not update", __METHOD__, $data);
                    $this->Common->setFlashErrorMessage(__("Can not update"));
                }
                $this->Common->setFlashSuccessMessage(__("Data updated successfuly"));
                if (empty($this->getParams())) {
                    return $this->redirect($this->request->here(false));
                }
                return $this->redirect($this->request->here(false) . '?' . http_build_query($this->getParams(), null, '&'));
            }
        }
    }
}