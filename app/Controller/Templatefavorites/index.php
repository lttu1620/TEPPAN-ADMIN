<?php

$modelName = $this->Templatefavorite->name;

//Process disable/enable
$this->doGeneralAction($modelName);

// create breadcrumb
$pageTitle = __('Favorite template list');
$this->Breadcrumb->setTitle($pageTitle)
	->add(array(
		'name' => $pageTitle,
	));

//get list of template
$templateList = MasterData::templates_all_key_value();

// Create search form 
$this->SearchForm->setModelName($modelName)
	->setAttribute('type', 'get')
	->addElement(array(
		'id' => 'name',
		'label' => __('Username')
	))
	->addElement(array(
		'id' => 'template_id',
		'label' => __('Template'),
		'options' => $templateList,
		'empty' => Configure::read('Config.StrAll'),
	))
	->addElement(array(
		'id' => 'mail',
		'label' => __('Mail')
	))
	->addElement(array(
		'id' => 'disable',
		'label' => __('Status'),
		'options' => array(
			0 => __('Favorite'),
			1 => __('Unfavorite')
		),
		'empty' => Configure::read('Config.StrAll'),
	))
	->addElement(array(
		'id' => 'sort',
		'label' => __('Sort'),
		'options' => array(
			'name-asc' => __('Username Asc'),
			'name-desc' => __('Username Desc'),
			'nickname-asc' => __('Nickname Asc'),
			'nickname-desc' => __('Nickname Desc'),
			'created-asc' => __('Date Asc'),
			'created-desc' => __('Date Desc'),
		),
		'empty' => Configure::read('Config.StrChooseOne'),
	))
	->addElement(array(
		'id' => 'limit',
		'label' => __('Limit'),
		'options' => Configure::read('Config.searchPageSize'),
	))
	->addElement(array(
		'type' => 'submit',
		'value' => __('Search'),
		'class' => 'btn btn-primary pull-right'
	));

$param = $this->getParams(array('page' => 1, 'limit' => Configure::read('Config.pageSize')));
if(!empty($id)){
    $param['template_id'] = $id;
}
list($total, $data) = Api::call(Configure::read('API.url_templatefavorites_list'), $param);
$this->set('total', $total);
$this->set('limit', $param['limit']);

$this->SimpleTable
	->addColumn(array(
		'id' => 'image',
		'type' => 'image',
		'title' => __('Image'),
		'src' => '{image}',
		'width' => '60'
	))
	->addColumn(array(
		'id' => 'username',
		'title' => __('Username'),
		'width' => 200,
		'empty' => '',
	))
	->addColumn(array(
		'id' => 'mail',
		'title' => __('Mail'),
		'empty' => '',
	))
	->addColumn(array(
		'id' => 'created',
		'title' => __('Date'),
		'type' => 'date',
		'width' => 120
	))
	->addColumn(array(
		'id' => 'disable',
		'type' => 'checkbox',
		'title' => __('Status'),
		'toggle' => true,
		'rules' => array(
			'0' => 'checked',
			'1' => ''
		),
		
		'empty' => 0,
		'width' => '100'
	))
	->setDataset($data);
