<?php

App::uses('AppController', 'Controller');

/**
 * BusinesscardsController class of Businesscards Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class BusinesscardsController extends AppController
{
    /**
     * Initializes components for BusinesscardsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Businesscards.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Businesscards/index.php');
    }

    /**
     * Handles user interaction of view update Businesscards.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Businesscards. Default value is 0.
     * @param int $isPrevUrl id prev url
     * @return void
     */
    public function update($id = 0, $isPrevUrl = 0)
    {
        include('Businesscards/update.php');
    }
}
