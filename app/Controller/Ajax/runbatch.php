<?php
if ($this->AppUI->is_admin) {
    Api::call(Configure::read('API.url_system_runbatch'));
    if (Api::getError()) {
        AppLog::info("Can not run batch", __METHOD__, Api::getError());
        echo implode('\n', Api::getError());	
    } else {
        echo __('Batch started successfuly');
    }
}
exit;