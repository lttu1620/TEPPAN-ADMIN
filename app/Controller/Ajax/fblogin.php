<?php
$this->Cookie->httpOnly = true;
$facebookInfo = $this->getParams();
$userInfo = Api::call(Configure::read('API.url_userrecruiters_login_facebook'), $facebookInfo); 
if (Api::getError()) {
    $this->Common->setFlashErrorMessage(Api::getError());
    echo $this->Auth->redirect('/login');
    exit;
}  
if ($this->createLoginSession($userInfo)) {
	$this->Cookie->write('last_login_url', "login", true, '2 weeks');
    echo $this->Auth->redirect();
}