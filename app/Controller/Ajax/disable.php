<?php
$param = $this->data;
$result = Api::call("{$param['controller']}/disable", $param);
if (empty($result) && !Api::getError()) {
    AppLog::warning("Can not update", __METHOD__, $param);
    echo __("System error, please try again");
}
exit;