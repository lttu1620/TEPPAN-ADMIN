<?php

App::uses('SimpleFormComponent', 'Component');

/**
 * 
 * create search form
 * @package ADM
 * @created 2014-11-27 
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class SearchFormComponent extends SimpleFormComponent {
    
}
