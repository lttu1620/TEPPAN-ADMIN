<?php

App::uses('AppComponent', 'Component');

/**
 * 
 * Render chart
 * @package Controller
 * @created 2014-11-26 
 * @version 1.0
 * @author thailvn
 * @copyright Oceanize INC
 */
class ChartComponent extends AppComponent {

    /** @var array $components Use components */
    public $components = array('Common');
    
    /**
     * Create javascript for chart
     *    
     * @author thailvn
     * @param array $param Params to create chart
     * @return string Javascript
     */
    public function render($param = array()) {        
        if (empty($param['legend'])) {
            $param['legend'] = array(
                'verticalAlign' => 'bottom'
            );
        }   
        if (empty($param['type'])) {
            $param['type'] = 'line';
        }        
        $chart = array(
            'credits' => array(
                'enabled' => false
            ),
            'chart' => array(
                'type' => $param['type']
            ),
            'legend' => $param['legend'],
            'yAxis' => array(
                'title' => array('text' => '')
            ),
        );
        if (!empty($param['title'])) {
            $chart['title'] = array(
                'text' => $param['title'],
                'x' => -20
            );
        }
        if (!empty($param['subtitle'])) {
            $chart['subtitle'] = array(
                'text' => $param['subtitle'],
                'x' => -20
            );
        }
        $series = $categories = array();
        foreach ($param['data'] as $item) {
            $categories[] = $item[$param['x']];            
        }
        foreach ($param['y'] as $key => $name) {
            $y['name'] = $name;
            $y['data'] = array();
            foreach ($param['data'] as $item) {
                $y['data'][] = (!empty($item[$key]) ? $item[$key] : 0) + 0;                
            }
            $series[] = $y;
        }
        $chart['xAxis'] = array('categories' => $categories);
        $chart['series'] = $series;
        $chart = json_encode($chart);
        return "
            <script type=\"text/javascript\">
                $(function() {
                    $('#{$param['id']}').highcharts({$chart});
                });
            </script>
        ";        
    }    
}
