<?php

$modelName = $this->Department->name;
$model = $this->{$modelName};
$data = array();
$pageTitle = __('Add department');
if (!empty($id)) {
    $pageTitle = __('Edit department');
    $param['id'] = $id;
    $data[$modelName] = Api::call(Configure::read('API.url_departments_detail'), $param);
    $this->Common->handleException(Api::getError());
}
$this->setPageTitle($pageTitle);
// create breadcrumb
$this->Breadcrumb->setTitle($pageTitle)
    ->add(array(
        'link' => $this->request->base . '/departments',
        'name' => __('Department list'),
    ))
    ->add(array(
        'name' => $pageTitle,
    ));

// create update form
$this->UpdateForm->setModelName($modelName)
    ->setData($data)
    ->addElement(array(
        'id' => 'id',
        'type' => 'hidden',
        'label' => __('Id'),
    ))
    ->addElement(array(
        'id' => 'university_id',
        'type' => 'hidden',
    ))
    ->addElement(array(
        'id' => 'university_name',
        'label' => __('University'),
        'options' => array(
            'url' => "/ajax/autocompleteuniversity",
            'callback' => "callbackCampuse",
        ),
        'onblur' => "onblurUniversity(this,'university_id');",
        'autocomplete_ajax' => true,
    ))
    ->addElement(array(
        'id' => 'name',
        'label' => __('Name'),
        'required' => true
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Save'),
        'class' => 'btn btn-primary pull-left',
    ))
    ->addElement(array(
        'type' => 'submit',
        'value' => __('Cancel'),
        'class' => 'btn btn-primary pull-left',
        'onclick' => 'return back();'
    ));

// process when submit form
if ($this->request->is('post')) {
    if ($model->validateInsertUpdate($this->getData($modelName))) {
        $id = Api::call(Configure::read('API.url_departments_addupdate'), $model->data[$modelName]);
        if (!empty($id) && !Api::getError()) {
            $this->Common->setFlashSuccessMessage(__('Data saved successfuly'));
            return $this->redirect("/{$this->controller}/update/{$id}");
        }
        // if validation error from api, write log and set validation error
        AppLog::info("Can not update", __METHOD__, $this->data);
        $model->setValidationErrors(Api::getError());
    }
    // show validation error
    $this->Common->setFlashErrorMessage($model->validationErrors);
}