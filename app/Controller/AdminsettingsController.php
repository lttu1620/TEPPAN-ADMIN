<?php
/**
 * AdminsettingsController class of Adminsettings Controller
 *
 * @package	Controller
 * @version 1.0
 * @author Tuan Cao
 * @copyright Oceanize INC
 */
class AdminsettingsController extends AppController {

     /**
     * Initializes components for AdminsettingsController class.
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Adminsettings.
     *
     * @param integer $admin_id ID value of Admin. Default value is 0.
     *
     * @return void
     */
    public function index($admin_id = 0) {
        include ('Adminsettings/index.php'); 
    }
}
