<?php

/**
 * UsersettingsController class of Usersettings Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserSettingsController extends AppController
{
    /**
     * Initializes components for UsersettingsController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Usersettings.
     *
     * @author Le Tuan Tu
     * @param integer $user_id ID value of Usersettings. Default value is 0.
     * @return void
     */
    public function index($user_id = 0)
    {
        include('Usersettings/index.php');
    }
}
