<?php

App::uses('AppController', 'Controller');

/**
 * System Controller
 * 
 * @package Controller
 * @created 2014-12-15
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class SystemController extends AppController
{
    public $uses = array('User', 'System');

    /**
     * Construct
     * 
     * @author tuancd 
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    /**
     * deletecache action
     * 
     * @author tuancd
     * @return void 
     */
    public function deletecache() {
        AppCache::delete(Configure::read('universities_all')->key);
        AppCache::delete(Configure::read('universities_all_key_value')->key);
        AppCache::delete(Configure::read('campuses_all')->key);
        AppCache::delete(Configure::read('departments_all')->key);
        AppCache::delete(Configure::read('mail_situations_all_key_value')->key);
        AppCache::delete(Configure::read('mail_types_all_key_value')->key);
        AppCache::delete(Configure::read('users_all_key_value')->key);
        AppCache::delete(Configure::read('templates_all_key_value')->key);
        AppCache::delete(Configure::read('report_general')->key);
        $this->Common->setFlashSuccessMessage(__('Cacche deleted successfuly'));
        return $this->redirect($this->request->referer());
    }
}
