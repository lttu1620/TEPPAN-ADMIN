<?php

App::uses('AppController', 'Controller');

/**
 * UsersController class of Users Controller
 *
 * @package Controller
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UsersController extends AppController
{
    /**
     * Initializes components for UsersController class.
     */
    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
    }

    /**
     * Handles user interaction of view index Users.
     *
     * @author Le Tuan Tu
     * @return void
     */
    public function index()
    {
        include('Users/index.php');
    }

    /**
     * Handles user interaction of view profile Users.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Users. Default value is 0.
     * @return void
     */
    public function profile($id = 0)
    {
        include('Users/profile.php');
    }

    /**
     * Handles user interaction of view signature Users.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Users. Default value is 0.
     * @return void
     */
    public function signature($id = 0)
    {
        include('Users/signature.php');
    }

    /**
     * Handles user interaction of view profileinformation Users.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Users. Default value is -1.
     * @return void
     */
    public function profileinformation($id = -1)
    {
        include('Users/profileinformation.php');
    }

    /**
     * Handles user interaction of view templates Users.
     *
     * @author Le Tuan Tu
     * @param integer $user_id ID value of Users.
     * @return void
     */
    public function templates($user_id)
    {
        if (empty($user_id))
            $this->redirect('index');
        include('Users/templates.php');
    }

    /**
     * Handles user interaction of view password Users.
     *
     * @author Le Tuan Tu
     * @param integer $id ID value of Users. Default value is 0.
     * @return void
     */
    public function password($id = 0)
    {
        if (empty($id)) {
            $id = $this->AppUI->id;
        }
        include('Users/password.php');
    }

    /**
     * Handles user interaction of view templatefavorites Users.
     *
     * @author Le Tuan Tu
     * @param integer $user_id ID value of Users. Default value is -1.
     * @return void
     */
    public function templatefavorites($user_id = -1)
    {
        include('Users/templatefavorites.php');
    }

    /**
     * Handles user interaction of view businesscards Users.
     *
     * @author Le Tuan Tu
     * @param integer $user_id ID value of Users. Default value is -1.
     * @return void
     */
    public function businesscards($user_id = -1)
    {
        include('Users/businesscards.php');
    }

}
